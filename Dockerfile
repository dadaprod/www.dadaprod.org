FROM debian:latest

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8 \
    && apt-get update && apt-get install -y apache2 python3-venv

ENV LANG fr_FR.utf8
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY . /srv/app
WORKDIR /srv/app

RUN pip install -r requirements.txt
RUN pelican -s publishconf.py
RUN mv public/* /var/www/html/

EXPOSE 80
CMD apachectl -D FOREGROUND -e info 
