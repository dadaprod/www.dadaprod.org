AUTHOR = 'dadaprod'
SITENAME = 'Dadaprod'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# customization
THEME = 'themes/dadaprod-theme'
STATIC_PATHS = ('images/', 'docs/', 'extra/robots.txt')
ARTICLE_EXCLUDES = ['include/album', 'include/single', 'include/video']
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'}
}

BOOTSTRAP_CSS_FILE = 'css/bootstrap.min.css'
BOOTSTRAP_DADA_CSS_FILE = 'css/bootstrap-dadaprod.css'
BOOTSTRAP_JS_FILE = 'js/bootstrap.min.js'
JQUERY_JS_FILE = 'js/jquery.min.js'
FAVICON_FILE = 'images/favicon.ico'

MENU = (
    ('Dadaprod', 'changeImg(1)', (
        ('Accueil', ''),
        ('', ''),
        ('À propos', 'pages/about.html'),
        ('Contact', 'pages/contact.html')
        )),
    ('Artistes', 'changeImg(0)', (
        ('Brazil', 'Brazil.html'),
        ('Banga', 'Banga.html'),
        ('Malaussene', 'Malaussene.html'),
        ('Michel Sardon', 'Michel-Sardon.html')
        )),
    ('Media', 'changeImg(2)', (
        ('Album', 'category/album.html'),
        ('Singl\'ogg', 'category/single.html'),
        ('Video clip', 'category/video.html'),
        ('', ''),
        ('Revolu\'Son', 'category/radio.html'),
        ('Samizdat Tv', 'category/samizdat.html')
        )),
    ('Mail-order', 'changeImg(4)', 'tag/mail-order.html')
)

ASIDE_FR = {
    'article': """\
<p>Depuis le début, dadaprod dénonce encore et toujours la marchandisation de la musique, qui sévit au sein des bien nommées industries du disque (rendez vous compte, pour un disque que vous achetez généralement entre 15 et 20 euros, l'artiste touche en moyenne 1,50 euros).</p>
<p>Néanmoins la création musicale nécessitant du temps, elle peut justifier quelques écarts au monde du travail classique, valeur autour de laquelle notre société est à l'heure actuelle organisée. Aussi, bien que cela ne corresponde pas à notre vision de la société idéale, qui alternerait pour chacun activités artistiques et contribution sociétale, nous envisageons de rémunérer un peu nos artistes à raison de 2 euros par disque (Il va de soit que nous préfèrons au maximum éviter l'utilisation d'argent, en favorisant les échanges divers et variés avec nos disques).</p>
<p>Aussi et afin d'améliorer également votre qualité d'écoute, nous vous encourageons vivement à commander les Cd-R de nos différents projets dans la section mail-order, tout en fournissant aux internautes des mp3 gratuits, afin et ce qui reste notre premier but, de diffuser la musique au plus grand nombre.</p>
<p>Enfin pour finir de vous convaincre, sachez que nous accordons énormément d'importance au graphisme de nos disques, chaque Cd-R etant ainsi une oeuvre artistique en soi, découpée et emballée à la main.</p>""",
    'page': """\
Dadaprod continue son œuvre de résistance sur Internet, en vous proposant encore et toujours des media libres, sans l'aide des GAFAM. Il y déjà quelques années, nous avions changé d'hébergeur; Là encore cela a rapport avec le monde libre, puisque l'hébergeur était la TuxFamily. Nous sommes désormais hébergé sur les pages Framagit.
    """
}

ASIDE_EN = {
    'article': """\
<p>Until the beginning,we have denounced the marchandisation of the well named disc industry (guess what! for a Cd that will cost you between 15 and 20 euros, the artist will only earn an average of 1,50 euros).</p>
<p>But, musical creation takes time, and can justify for a while, a little break with what they call job, which is unfortunately around what our society is based on. Thus, even if that does not correspond to our vison of the perfect society, which would enable for all of us both artistic and community work, we would like to give a little contibution to our artists, regarding as 2 euros per Cd (Obviously, we would like as far as possible to avoid using money, so we expecting people to propose some various exchange around our Cd).</p>
<p>Finally and also in order to improve the quality of listening, we support you to order our different cdr in our mail order section, proposing in the same time some free mp3, cause our first aim still is making this music more popular.</p>
<p>If You are not already convinced, we finally add that we working hard on the graphics part of our dics, each Cd-R is an artistic piece, cut and pack by our hands.</p>""",
    'page': """\
Dadaprod still resist for a Internet without GAFAM, publishing free media. A couple of years ago, we have changed for our hosting platform; TuxFamily are using more free software (see also the link section). There are hosting our website without asking money for. Framagit pages are now hosting us. We invite you for helping both of them with your gift."""
}

MARKDOWN = {
    "extension_configs": {
        # Needed for code syntax highlighting
        "markdown.extensions.codehilite": {},
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
    },
    "output_format": "html5",
}

