Title: Marche mondiale pour le climat
Date: 2016-11-12 00:00
Category: samizdat
Keywords: climat, montage video, Creative Commons, libre
Slug: marche-mondiale-pour-le-climat
Lang: fr
Summary: Captation de la marche mondiale pour le climat du 12 novembre 2016...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/marche-climat.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/marche-climat.html){:onClick="ff=window.open('/theme/lecteur/marche-climat.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.webm (350 Mo)](http://www.archive.org/download/dadaprod_samizdat_Marche-Climat_CC-by-sa_2016/dadaprod_samizdat_Marche-Climat_CC-by-sa_2016_medium.webm){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Marche mondiale pour le climat
{: .titre .text-center}

Les citoyens du monde entier qui s'inquiètent du dérèglement climatique
sont descendus dans la rue, tout comme l’on fait les militants de l’agglomération
vichyssoise dans le cadre de la marche mondiale pour le climat, le 12 novembre 2016.

Bien que ces marches ont notamment pour but d’encourager les négociateurs à
signer un accord ambitieux, équitable et contraignant pour sauver le climat,
nous ne sommes pas dupes et nous nous mobilisons pour que le plus grand
nombre prenne conscience des enjeux afin de prendre des résolutions individuelles,
mais aussi aussi d’agir collectivement pour une mise en œuvre de solutions
pragmatiques sans attendre des accords contraignants qui ne viendront peut-être jamais.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2016 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
