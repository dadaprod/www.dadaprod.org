Title: profs en danger
Date: 2008-04-01 00:00
Category: samizdat
Keywords: grèves lycée, profs, syndicats, luttes, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: profs-en-danger
Lang: fr
Summary: Captation d'une destruction calme et structurée d'un service public...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/profsendanger.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/profs-en-danger.html){:onClick="ff=window.open('/theme/lecteur/profs-en-danger.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (46 Mo)](https://archive.org/download/ProfsEnDanger/dadaprod.samizdat_prof.en.danger_CC.by.nc-sa_2008_512kb.ogv){:download}
[.ogv pro (162 Mo)](https://archive.org/download/ProfsEnDanger/dadaprod.samizdat_prof.en.danger_CC.by.nc-sa_2008.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

profs en danger
{: .titre .text-center}

A l'heure où la bling bling-isation bat son plein, le gouvernement et le Medef
nous demande de faire des efforts car bien sûr la France va mal : ils ne font
pas assez de bénéfices (sur notre dos), ils ne polluent pas encore assez la planète.

Les personnels de l'éducation sont particulièrement visés par cette aspiration
à l'économie. Les médias dominants relaient ces fausses informations :
l'éducation est un budget énorme qui cause le déficit.

La france n'est pas en faillite et l'éducation est un investissement.

Au niveau plus local, certains recteurs font du zèle et proposent de faire
des économies en supprimant des postes. C'est le cas à Créteil (et ailleurs)
où l'on propose à certains enseignants d'aller voir ailleurs tout en proposant à
d'autres de faire des heures supplémentaires: car le service à faire reste le même !

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2008 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 3.0](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
