Title: Le Triple A
Date: 2012-01-30 00:00
Category: samizdat
Keywords: crise, dette, audit citoyen, banque, moralisation, capitalisme, triple A, festival, court métrage, Auvergne, Clermont Ferrand, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: le-triple-A-cac63
Lang: fr
Summary: Manifestation pour un audit citoyen de la dette publique à l'occasion du festival du court métrage...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/cac63_2.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/triple-A.html){:onClick="ff=window.open('/theme/lecteur/triple-A.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (42 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod_samizdat_Le-Triple-A_Art-Libre_2012.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Le Triple A
{: .titre .text-center}

Le collectif Puy-Dômois pour un audit citoyen de la dette publique (CAC63)
a déambulé le samedi 28 janvier 2012 à Clermont-Ferrand, dans le hall de
la maison des congrès où avait lieu le festival du court métrage.
Les différents panneaux reprenant les 3 lettres AAA (le triple A) ont
provoqués des réactions plutôt mitigées du public, mais l’interrogation
ainsi suscitée va sûrement faire son chemin !

La captation vidéo est accompagnée du morceau [triple A]({filename}/articles/album/les-champs-de-la-resistance_fr.md)
de Michel Sardon.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2012 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
