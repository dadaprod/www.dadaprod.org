Title: pastiflu
Date: 2009-10-17 00:00
Category: samizdat
Keywords: Clowns, grippe, H1N1, pastiflu, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: pastiflu
Lang: fr
Summary: Action militante et humoristique des Clowns à Responsabilité Sociale (CRS)...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/pastiflu.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/pastiflu.html){:onClick="ff=window.open('/theme/lecteur/pastiflu.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (196 Mo)](https://archive.org/download/SamizdatTv/dadaprod.samizdat.pastiflu_Art.Libre_2009.ogv){:download}
[.ogv pro (328 Mo)](https://archive.org/download/SamizdatTv/Dadaprod_samizdat_pastiflu_Art.Libre_2009_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

pastiflu
{: .titre .text-center}

Les Clowns à Responsabilité Sociale (CRS) ont aidé Roseline Bachelot. En
novembre 2009, ils ont vendu un kit de vaccination (Pastiflu, conseils et
kits de survie) contre la grippe au marché Saint-Pierre à Clermont-Fd.
Une fois de plus, les CRS ont télescopé le premier degré des discours politiques au monde réel.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
