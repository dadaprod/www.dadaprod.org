Title: déboulonnons les publicitaires
Date: 2005-11-25 00:00
Category: samizdat
Keywords: anti-pub, déboulonneurs, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: deboulonnons-les-publicitaires
Lang: fr
Summary: Première action anti-pub du collectif des déboulonneurs parisiens...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/deboulonnons_1.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/deboulonnons_1.html){:onClick="ff=window.open('/theme/lecteur/deboulonnons_1','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (38 Mo)](https://archive.org/download/DeboulonnonsLesPublicitaires/dadaprod.samizdat_deboulonnons.les.publicitaires.01_CC.by.nc-sa_2005_512kb.ogv){:download}
[.mp4 (58 Mo)](https://archive.org/download/DeboulonnonsLesPublicitaires/dadaprod.samizdat.deboulonnons.les.publicitaires_01.CC.by.nc-sa_2005.mp4){:download}
[.ogv (89 Mo)](https://archive.org/download/DeboulonnonsLesPublicitaires/dadaprod.samizdat_deboulonnons.les.publicitaires.01_CC.by.nc-sa_2005.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

déboulonnons les publicitaires
{: .titre .text-center}

Le 25 novembre 2005, une centaine de personnes, répondant à l'appel du **collectif
des déboulonneurs** se rassemble à l'angle de la rue Drouot et du Bd Montmartre
dans le 9ème arrondissement parisien.

Cette action anti-pub au grand jour vise à alerter le législateur pour que
la taille des panneaux publicitaires soit limitée à **50** par **70** cm.
Les autres actions légales ayant toutes échoué, le collectif des déboulonneurs
se trouve ainsi forcé de troubler l'ordre public, par des actions non violentes,
recherchant ainsi un jugement au pénal, pour porter ses idées devant la justice française.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2005 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
