Title: Convergence des luttes
Date: 2004-05-01 00:00
Category: samizdat
Keywords: manifestation, occupation, résistance, luttes, mayday, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: convergence-des-luttes
Lang: en
Summary: Mayday is day where everyone is supposed either to demonstrate or have a rest, but no one is supposed to work...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/mayday_2004.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/mayday_2004.html){:onClick="ff=window.open('/theme/lecteur/mayday_2004','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (49 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004_512kb.ogv){:download}
[.mp4 (66 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004.mp4){:download}
[.ogv (153 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Convergence des luttes
{: .titre .text-center}

Mayday is day where everyone is supposed either to demonstrate or have a
rest, but no one is supposed to work, that’s why some people has decided
an action to help some workers in a fast food to fight for their rights... 

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}

[CC by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en){: target="_blank"}
{: .text-center}
