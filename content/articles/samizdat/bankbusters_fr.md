Title: bankbusters
Date: 2011-06-29 00:00
Category: samizdat
Keywords: banque, moralisation, capitalisme, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: bankbusters
Lang: fr
Summary: Action de rue militante des bankbusters pour nettoyer des banques de la place de Jaude...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/bankbusters.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/bankbusters.html){:onClick="ff=window.open('/theme/lecteur/bankbusters.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (63 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.samizdat.attac.la.banque_Art.Libre_2011_2Mbs.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

bankbusters
{: .titre .text-center}

Face aux dérives dévastatrices et incontrôlées du monde de la finance, Attac63
a fait appel aux bankbusters pour agir contre les activités paranormales des banques !
Une action de rue choc et sympa de nettoyage des banques de la place de Jaude à Clermont-Fd samedi 21 mai 2011. 

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2011 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
