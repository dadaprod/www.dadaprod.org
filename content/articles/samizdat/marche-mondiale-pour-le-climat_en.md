Title: Marche mondiale pour le climat
Date: 2016-11-12 00:00
Category: samizdat
Keywords: climat, dadaprod, Creative Commons, free
Slug: marche-mondiale-pour-le-climat
Lang: en
Summary: march for the climat on the 12th of novembrer 2016 in Vichy...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/marche-climat.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/marche-climat.html){:onClick="ff=window.open('/theme/lecteur/marche-climat.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.webm (350 Mo)](http://www.archive.org/download/dadaprod_samizdat_Marche-Climat_CC-by-sa_2016/dadaprod_samizdat_Marche-Climat_CC-by-sa_2016_medium.webm){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Marche mondiale pour le climat
{: .titre .text-center}

march for the climat on the 12th of novembrer 2016 in Vichy. 


![cc by sa 3.0]({static}/images/cc-by-sa_88x31.png)
{: .text-center}

[CC by sa](http://creativecommons.org/licenses/by-sa/3.0/deed.en){: target="_blank"}
{: .text-center}
