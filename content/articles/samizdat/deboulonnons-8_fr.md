Title: deboulonnons #8
Date: 2006-06-24 00:00
Category: samizdat
Keywords: barbouilleurs, déboulonneurs, action, reportage, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: deboulonnons-8
Lang: fr
Summary: montage vidéo sur la huitième action des déboulonneurs parisiens...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/deboulonnons_8.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/deboulonnons_8.html){:onClick="ff=window.open('/theme/lecteur/deboulonnons_8','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (43 Mo)](https://archive.org/download/Deboulonnons8laSuite../dadaprod.samizdat_deboulonnons.les.publicitaires.08_CC.by.nc-sa_2006_512kb.ogv){:download}
[.mp4 (65 Mo)](https://archive.org/download/Deboulonnons8laSuite../dadaprod.samizdat.deboulonnons.les.publicitaires_08.CC.by.nc-sa_2006.mp4){:download}
[.ogv (145 Mo)](https://archive.org/download/Deboulonnons8laSuite../dadaprod.samizdat_deboulonnons.les.publicitaires.08_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

deboulonnons #8
{: .titre .text-center}

Alors que va se tenir le 27 juin 2006 à Montpellier le premier procès de
deux barbouilleurs, le collectif des **déboulonneurs** parisien avait invité,
en ce vendredi 23 juin 2006, les citoyens et citoyennes à venir les soutenir
dans leur action de désobéissance civile. Le collectif souhaite par ces actions
ramener l'affiche publicitaire à un format de **50** par **70** cm. Dadaprod
était là et vous propose un reportage vidéo sur cette action...

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
