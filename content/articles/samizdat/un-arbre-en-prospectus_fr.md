Title: Un arbre en prospectus
Date: 2011-11-27 00:00
Category: samizdat
Keywords: Brazil, antipub, Clermont Ferrand, Auvergne, arbre, papier, prospectus, boite à lettre, pollution, environnement, décroissance, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: un-arbre-en-prospectus
Lang: fr
Summary: Action antipub collaborative...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/GRAP-a-Jaude.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/un-arbre-en-prospectus.html){:onClick="ff=window.open('/theme/lecteur/un-arbre-en-prospectus.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (61 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.samizdat.un-arbre-en-prospectus_Art.Libre_2011.ogv){:download}
[.ogv pro (96 Mo)](https://archive.org/download/SamizdatTv/Dadaprod_samizdat_un-arbre-en-prospectus_Art.Libre_2011_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Un arbre en prospectus
{: .titre .text-center}

Cette action du Groupe de Résistant-e-s AntiPub (GRAP) de Clermont-Ferrand,
qui a eu lieu place de Jaude le 26 novembre 2011, a consisté à construire
un arbre en grillage et pâte à papier. Cet arbre symbolise l'équivalent
en prospectus qui arrive dans nos boites à lettre tous les ans. L'invitation
est faite aux passants de mettre la main à la pâte, parallèlement des affiches
Decaux ont été mises à disposition à des fins de détournement ou pour y écrire des graffitis.

Cette captation vidéo est mise en musique avec le morceau "graffiti" de **Brazil**,
présent sur l'album [la faim du travail]({filename}/articles/album/la-faim-du-travail_fr.md).

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2011 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
