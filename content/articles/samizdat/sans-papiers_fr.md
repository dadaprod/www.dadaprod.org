Title: sans-papiers
Date: 2002-05-24 00:00
Category: samizdat
Keywords: sans-papiers, médias, indépendant, video, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: sans-papiers
Lang: fr
Summary: Les sans-papiers sont bien aussi souvent des sans-voix, sans toit, sans couverture médiatique...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/sans_papiers.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/sans-papiers.html){:onClick="ff=window.open('/theme/lecteur/sans-papiers','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (31 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003_512kb.ogv){:download}
[.ogv (128 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003.ogv){:download}
[.ogv pro (418 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

sans-papiers
{: .titre .text-center}

Les sans-papiers sont bien aussi souvent des sans-voix, sans toit, sans couverture médiatique.

Les médias classiques préférant laisser la parole à la répression Sarkozienne,
il revient donc aux médias dit indépendants (des lobbies militaro-financiers)
de couvrir ces sujets jugés peu fédérateurs..., en témoigne ce reportage filmé
à Paris en 2003 lors d'une manifestation de sans-papiers.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2002 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
