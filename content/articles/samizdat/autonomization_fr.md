Title: autonomization
Date: 2005-07-28 00:00
Category: samizdat
Keywords: autonomie, autogestion, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: autonomization
Lang: fr
Summary: L’autonomie est le pire cauchemar du monde capitaliste...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/autonomization.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/autonomization.html){:onClick="ff=window.open('/theme/lecteur/autonomization','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (69 Mo)](https://archive.org/download/autonomization/dadaprod.samizdat_automization_CC.by.sa_2005_512kb.ogv){:download}
[.mp4 (104 Mo)](https://archive.org/download/autonomization/dadaprod.samizdat.automization.CC.by.nc-sa_2005.mp4){:download}
[.ogv (168 Mo)](https://archive.org/download/autonomization/dadaprod.samizdat_automization_CC.by.sa_2005.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

autonomization
{: .titre .text-center}

L’autonomie est le pire cauchemar du monde capitaliste. Le simple fait de
penser qu’une personne puisse être indépendante de son emprise lui provoque
une horrible migraine. C’est la raison pour laquelle toutes les initiatives
en rapport avec l’émancipation du citoyen vis-à-vis du système actuel,
telles que l’autogestion, sont systématiquement passées sous silence,
voire même violemment torpillées dans certains cas. Cette tournée sera
donc une occasion idéale pour faire exploser la chape de plomb ploutocratique
qui nous asphyxie chaque jour un peu plus.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2005 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
