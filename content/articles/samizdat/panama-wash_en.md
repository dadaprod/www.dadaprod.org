Title: Panama Wash
Date: 2016-06-29 00:00
Category: samizdat
Keywords: tax, bank, Attac, dadaprod, samizdat, activism, theora, ogv, Creative Commons, free
Slug: panama-wash
Lang: en
Summary: Activism against tax evasion. We have tried to clean the bank...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/panama-wash.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/panama-wash.html){:onClick="ff=window.open('/theme/lecteur/panama-wash.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.webm (70 Mo)](https://ia601602.us.archive.org/14/items/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium.webm){:download}
[.webm (229 Mo)](http://www.archive.org/download/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_high.webm){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Panama Wash
{: .titre .text-center}

Activism against tax evasion. We have tried to clean the bank...

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}

[CC by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.en){: target="_blank"}
{: .text-center}
