Title: CAC 63 #1
Date: 2011-11-26 00:00
Category: samizdat
Keywords: audit citoyen, dette publique, déficit, 63, puy de dôme, Clermont Ferrand, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: cac63-1
Lang: fr
Summary: Rassemblement du CAC 63 pour un audit citoyen de la dette publique...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/cac63_1.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/cac63-1.html){:onClick="ff=window.open('/theme/lecteur/cac63-1.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (80 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.samizdat.CAC63-1_Art.Libre_2011.ogv){:download}
[.ogv pro (153 Mo)](https://archive.org/download/SamizdatTv/Dadaprod_samizdat_CAC63-1_Art.Libre_2011_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

CAC 63 #1
{: .titre .text-center}

Les organisations membres du Collectif 63 pour un audit citoyen de la dette
publique ont appellées à manifester le samedi 26 novembre 2011.

Ce rassemblement, qui a eu lieu place de Jaude à Clermont-Ferrand, s’inscrit
dans l’action du Collectif national du même nom qui veut permettre aux citoyens
de se réapproprier la question de la dette publique, et à partir de là les
grandes questions de politique économique, comme des enjeux démocratiques.

Le Collectif entend aussi dénoncer les politiques injustes qui font payer
la crise de la finance par les peuples, et en particulier les catégories
les plus précaires et les plus défavorisées.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2011 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
