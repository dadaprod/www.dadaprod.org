Title: Retour à l'envoyeur
Date: 2010-03-26 00:00
Category: samizdat
Keywords: action, grap, publicité, environnement, antipub, boite à lettres, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: retour-a-l-envoyeur
Lang: fr
Summary: Action militante antipub consistant à rendre aux supermarchés les publicités qui inondent nos boites aux lettres...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/GRAP_Leclerc_06.03.2010.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/retour-a-l-envoyeur.html){:onClick="ff=window.open('/theme/lecteur/retour-a-l-envoyeur.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (25 Mo)](https://archive.org/download/RetourALenvoyeur/dadaprod.samizdat.retour.a.envoyeur_Art.Libre_2010_512kb.ogv){:download}
[.ogv (73 Mo)](https://archive.org/download/RetourALenvoyeur/dadaprod.samizdat.retour.a.envoyeur_Art.Libre_2010.ogv){:download}
[.ogv pro (223 Mo)](https://archive.org/download/RetourALenvoyeur/dadaprod.samizdat.retour.a.envoyeur_Art.Libre_2010_pro.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Retour à l'envoyeur
{: .titre .text-center}

Le 6 mars 2010, une vingtaine de militants contre la publicité commerciale
a répondu à l'invitation du GRAP (**G**roupe de **R**ésistance à l'**A**gression **P**ublicitaire)
afin de rendre aux supermarchés les publicités qui inondent nos boites aux lettres.

En effet, tous les ans, c'est un arbre que nous retrouvons dans notre boîte à lettre. En plus du coup environnemental, à titre individuel, chacun doit payer 500 euros par an pour la publicité diffusée sur tous les supports confondus.

**La gratuité via la publicité est un leurre** !

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2010 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
