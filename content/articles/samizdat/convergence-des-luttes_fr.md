Title: Convergence des luttes
Date: 2004-05-01 00:00
Category: samizdat
Keywords: manifestation, occupation, résistance, luttes, mayday, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: convergence-des-luttes
Lang: fr
Summary: En marge des défilés traditionnels du 1er mai, une action d’occupation d’un fast-food rassemble des exclus du système...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/mayday_2004.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/mayday_2004.html){:onClick="ff=window.open('/theme/lecteur/mayday_2004','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (49 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004_512kb.ogv){:download}
[.mp4 (66 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004.mp4){:download}
[.ogv (153 Mo)](https://archive.org/download/MayDay_920/dadaprod.samizdat_mayday_CC.by.sa_2004.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Convergence des luttes
{: .titre .text-center}

Dans un monde libéral, où la société de consommation a été peu à peu remplacée
par la société de sur consommation, tout est désormais jetable, du Dvd au salarié,
tout est optimisé pour que la croissance du portefeuille de quelques se maintienne
à un maximum durable, euphémisme emprunté aux écologistes pour désigner en fait
le seuil de tolérance qui fait passer un peuple de la soumission à la révolution.

C’est dans ce contexte que s’organise la convergence des luttes, qui vise
à rassembler les exclus de ce système inique que l’on appelle encore démocratie.
Le 1er Mai apparaissant comme un jour de résistance emblématique, le reportage
nous amène des défilés des cortèges traditionnels vers une action d’occupation
d’un fast-food, dont les propriétaires ne se gênent pas pour exploiter quelques
malheureux en ce jour de fête des travailleurs…

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2004 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
