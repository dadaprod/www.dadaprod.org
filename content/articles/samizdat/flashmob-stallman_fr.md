Title: flashmob Stallman
Date: 2006-06-12 00:00
Category: samizdat
Keywords: dadvsi, GPL, Stallman, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: flashmob-stallman
Lang: fr
Summary: Mobilisation du collectif stopdrm contre le projet de loi Dadvsi...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/flashmob_stallman.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/flashmob-stallman.html){:onClick="ff=window.open('/theme/lecteur/flashmob-stallman','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (23 Mo)](https://archive.org/download/FlashmobStallman/dadaprod.samizdat_flashmob.stallman_CC.by.nc-sa_2006_512kb.ogv){:download}
[.mp4 (32 Mo)](https://archive.org/download/FlashmobStallman/dadaprod.samizdat.flashmob.stallman.CC.by.nc-sa_2006.mp4){:download}
[.ogv (78 Mo)](https://archive.org/download/FlashmobStallman/dadaprod.samizdat_flashmob.stallman_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

flashmob Stallman
{: .titre .text-center}

Vendredi 9 juin 2006, alors que le gouvernement français hésite encore entre
accorder ou pas une deuxième lecture au projet de loi Dadvsi, **Richard Stallman**,
créateur entre autre de la licence GPL nous fait l'honneur de sa visite.
Le président de la Free Software Fondation (FSF) s'est joint au collectif stopdrm,
qui avait organisé une mobilisation dans un supermarché culturel pour essayer
encore une fois d'alerter leurs concitoyens sur le caractère liberticide de ce projet de loi.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
