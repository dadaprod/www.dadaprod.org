Title: Marche anti DADVSI
Date: 2006-05-07 00:00
Category: samizdat
Keywords: dadvsi, DRM, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: marche-anti-dadvsi
Lang: fr
Summary: Une marche pour les libertés numériques, contre les DRM et contre la loi DADVSI...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/manif_drm.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/manif-drm.html){:onClick="ff=window.open('/theme/lecteur/manif-drm','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (26 Mo)](https://archive.org/download/MarcheAntiDadvsi/dadaprod.samizdat_marche.contre.Dadvsi_CC.by.nc-sa_2006_512kb.ogv){:download}
[.mp4 (37 Mo)](https://archive.org/download/MarcheAntiDadvsi/dadaprod.samizdat.marche.contre.Dadvsi.CC.by.nc-sa_2006.mp4){:download}
[.ogv (92 Mo)](https://archive.org/download/MarcheAntiDadvsi/dadaprod.samizdat_marche.contre.Dadvsi_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Marche anti DADVSI
{: .titre .text-center}

Le 7 mai 2006, a eu lieu à Paris, une marche pour les libertés numériques,
contre les DRM et contre la loi DADVSI. Dadaprod était là et vous présente
ce montage qui montre que la résistance est en place : nous ne laisserons
pas l'industrie de la culture dicter les lois !

Ce reportage vidéo est mise en musique avec le morceau [Stop DRM]({filename}/articles/single/stop-drm_fr.md)
de Michel Sardon...

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
