Title: CAC 63 #2
Date: 2011-12-17 00:00
Category: samizdat
Keywords: audit citoyen, dette publique, déficit, 63, puy de dôme, Clermont Ferrand, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: cac63-2
Lang: fr
Summary: Action du CAC 63 pour soutenir l’opération "les Pères Noêl s’occupent des banques"...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/cac63_1.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/cac63-2.html){:onClick="ff=window.open('/theme/lecteur/cac63-2.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (68 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.samizdat.CAC63-2_Art.Libre_2011.ogv){:download}
[.ogv pro (169 Mo)](https://archive.org/download/SamizdatTv/Dadaprod_samizdat_CAC63-2_Art.Libre_2011_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

CAC 63 #2
{: .titre .text-center}

Le CAC 63, collectif pour l’audit citoyen de la dette, rassemblant des syndicats
et des associations avec le soutien de partis politiques ont invité à un
rassemblement le Samedi 17 décembre 2011, place de Jaude, à Clermont-Ferrand.

A cette occasion, les clermontois ont pu soutenir l’opération "les Pères Noêl
s’occupent des banques". Une bonne action alors que l’on apprend que les dirigeants
des banques françaises ont vu leur rémunération augmenter de seulement 44.8% en 2010.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2011 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
