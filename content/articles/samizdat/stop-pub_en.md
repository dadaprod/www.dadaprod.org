Title: Stop Pub
Date: 2003-12-19 00:00
Category: samizdat
Keywords: Stop Pub, brazil, dadaprod, samizdat, activism, theora, ogv, Creative Commons, free
Slug: stop-pub
Lang: en
Summary: Stop pub action in Paris...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/stop_pub.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/stop-pub.html){:onClick="ff=window.open('/theme/lecteur/stop-pub','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (19 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004_512kb.ogv){:download}
[.ogv (44 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004.ogv){:download}
[.ogv pro (192 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Stop Pub
{: .titre .text-center}

This is a video, which has been recorded in Paris during the Stop pub action
on the 19th december 2003. A cut-off from different interviews taped during
the same action has been added afterwards.
The music is composed by [**brazil**]({filename}/articles/artiste/Brazil_fr.md),
which are using these images during their show... 

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}

[CC by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en){: target="_blank"}
{: .text-center}
