Title: Velorution
Date: 2010-07-09 00:00
Category: samizdat
Keywords: velo,  environnement, pollution, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: velorution-2009
Lang: fr
Summary: Vélorution dans les rues de Clermont-Ferrand...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/velorution.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/velorution-2009.html){:onClick="ff=window.open('/theme/lecteur/velorution-2009.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (63 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.samizdat.velorution_Art.Libre_2010_2mbps.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Velorution
{: .titre .text-center}

Juin 2009, quelques semaines après la fête du vélo organisée localement par
l'association vélocité 63, des cyclistes urbains entreprennent une ballade
moins familiale sur des axes routiers peu propices au déplacement doux...  

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2010 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
