Title: Le père Noël fait des ordures
Date: 2006-12-27 00:00
Category: samizdat
Keywords: collectif, déboulonneurs, antipub, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: le-pere-noel-fait-des-ordures
Lang: fr
Summary: A quelques jours de leur premier procès, le collectif des déboulonneurs parisiens avait appelé à un rassemblement...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/deboulonnons_12.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/deboulonnons_12.html){:onClick="ff=window.open('/theme/lecteur/deboulonnons_12','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (36 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/\[dadaprod\].\[samizdat\].deboulonnons.les.publicitaires.12_CC.by.nc-sa_2006_512kb.ogv){:download}
[.ogv (100 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/\[dadaprod\].\[samizdat\].deboulonnons.les.publicitaires.12_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Le père Noël fait des ordures
{: .titre .text-center}

A quelques jours de leur premier procès (qui aura lieu le 12 janvier 2007),
le collectif des déboulonneurs parisiens avait appelé à un rassemblement
le 23 décembre 2006 sur le parvis de l'hôtel de ville de Paris...

Plus d'infos sur le [site des déboulonneurs](https://www.deboulonneurs.org/){: target="_blank"}... 

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
