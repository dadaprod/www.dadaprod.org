Title: La journée de la goulotte
Date: 2010-04-02 00:00
Category: samizdat
Keywords: action, vélo, déplacement doux, environnement, urbain, sncf, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: la-journee-de-la-goulotte
Lang: fr
Summary: Action militante visant à dénoncer le manque d'engagement de la SNCF vis-à-vis des cyclistes...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/goulotte.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/goulotte.html){:onClick="ff=window.open('/theme/lecteur/goulotte.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (24 Mo)](https://archive.org/download/LaJourneeDeLaGoulotte/dadaprod.samizdat.la.journee.de.la.goulotte_Art.Libre_2010_512kbs.ogv){:download}
[.ogv (69 Mo)](https://archive.org/download/LaJourneeDeLaGoulotte/dadaprod.samizdat.la.journee.de.la.goulotte_Art.Libre_2010.ogv){:download}
[.ogv pro (206 Mo)](https://archive.org/download/LaJourneeDeLaGoulotte/dadaprod.samizdat.la.journee.de.la.goulotte_Art.Libre_2010_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

La journée de la goulotte
{: .titre .text-center}

Après la goulotte rouillée, adressée par l'association Vélocité 63 en 2009, à
la direction auvergnate de la SNCF; Vélocité 63 enfonce le clou sous forme de poisson d'avril.

En effet, en ce 1er avril 2010, les militants de l'association ont décidé de
faire ce que la SNCF refuse de faire depuis des années, à savoir, installer
une goulotte le long d'un escalier menant aux quais afin de transporter plus
facilement les vélos, mais aussi les valises et les poussettes... 

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2010 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
