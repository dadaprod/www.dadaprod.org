Title: Panama Wash
Date: 2016-06-29 00:00
Category: samizdat
Keywords: PanamaPapers, Attac, banque, crise, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: panama-wash
Lang: fr
Summary: Montage vidéo sur une opération "Banque Propre"...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/panama-wash.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/panama-wash.html){:onClick="ff=window.open('/theme/lecteur/panama-wash.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.webm (70 Mo)](https://ia601602.us.archive.org/14/items/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium.webm){:download}
[.webm (229 Mo)](http://www.archive.org/download/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_medium/dadaprod_samizdat_Panama-Wash_CC-by-sa_2016_high.webm){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Panama Wash
{: .titre .text-center}

Une opération "Banque Propre" a été menée à Vichy. La société de nettoyage
Panama Wash montée pour l’occasion est donc intervenue gratuitement auprès
de l’agence de la Société Générale de Vichy.

Action mise en scène par [Attac 03 (Vichy)](http://local.attac.org/vichy/spip.php?article62){: target="_blank"}. 

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2016 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
