Title: Pas de pub à la télé pour les moins de 12 ans
Date: 2009-10-07 00:00
Category: samizdat
Keywords: action, grap, publicité, antipub, télé, enfant, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: pas-de-pub-a-la-tv
Lang: fr
Summary: Refusons le matraquage publicitaire pour les plus jeunes...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/pas-de-pub-tele-pour-les-moins-12-ans.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/pas-de-pub-a-la-tv.html){:onClick="ff=window.open('/theme/lecteur/pas-de-pub-a-la-tv.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (10 Mo)](https://archive.org/download/SamizdatTv/dadaprod.samizdat.pas-de-pub-tele-pour-les-moins-12-ans_Art.Libre_2009.ogv){:download}
[.ogv pro (18 Mo)](https://archive.org/download/SamizdatTv/Dadaprod_samizdat_pas-de-pub-tele-pour-les-moins-12-ans_Art.Libre_2009_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Pas de pub à la télé pour les moins de 12 ans
{: .titre .text-center}

Comment un jeune enfant peut-il résister aux spots publicitaires qui lui
vantent sans arrêt des jouets, des sodas pétillants, des crèmes onctueuses,
des vêtements de marque, le téléphone portable dernier cri… ? Non seulement
le jeune enfant ne fait pas la différence entre la réalité et la fiction,
mais il ne comprend pas la finalité commerciale des publicités qui le matraque
pour le formater - jusqu’à 60 spots le mercredi matin ! Ces publicités dictent
à l’enfant des comportements auxquels ses parents vont être confrontés :
tyrannie des marques, grignotage, obésité, perte des valeurs… La publicité
à la télévision nuit gravement à la santé et à l’éducation.

Le pouvoir législatif ayant reculé en 2009, le Mouvement pour une Alternative
Non-Violente ([MAN](https://nonviolence.fr/){: target="_blank"}) a initié
une pétition nationale sur le sujet...

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
