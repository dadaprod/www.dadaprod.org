Title: Détrônons les étrons
Date: 2006-10-30 00:00
Category: samizdat
Keywords: collectif, déboulonneurs, publicitaire, procès, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: detronons-les-etrons
Lang: fr
Summary: Il aura fallu dix actions pour que le déboulonneurs parisiens obtiennent enfin une procès à Paris...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/deboulonnons_11.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/deboulonnons_11.html){:onClick="ff=window.open('/theme/lecteur/deboulonnons_11','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (30 Mo)](https://archive.org/download/DetrononsLesEtrons/dadaprod.samizdat.deboulonnons.les.publicitaires_10.CC.by.nc-sa_2006.ogv){:download}
[.mp4 (49 Mo)](https://archive.org/download/DetrononsLesEtrons/dadaprod.samizdat.deboulonnons.les.publicitaires_10.CC.by.nc-sa_2006.mp4){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Détrônons les étrons
{: .titre .text-center}

Il aura fallu dix actions pour que le déboulonneurs parisiens obtiennent
enfin un **procès** à Paris, devant le tribunal correctionnel (le 12 janvier
2007). Pour le collectif, cela constitue bien sûr une victoire, car ce
procès sera une occasion de pousser le législateur à changer la loi !
Rappelons que le collectif des déboulonneurs réclame que la taille de
l'affichage publicitaire soit limité à **50** par **70** centimètres.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
