Title: sans papiers
Date: 2002-05-24 00:00
Category: samizdat
Keywords: illegal immigrants, indy medias, video, dadaprod, samizdat, activism, theora, ogv, Creative Commons, free
Slug: sans-papiers
Lang: en
Summary: Illegal immigrants are also very often homeless and without media coverage...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/sans_papiers.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/sans-papiers.html){:onClick="ff=window.open('/theme/lecteur/sans-papiers','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (31 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003_512kb.ogv){:download}
[.ogv (128 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003.ogv){:download}
[.ogv pro (418 Mo)](https://archive.org/download/SansPapiers/dadaprod.samizdat.sans.papiers_CC.by.sa_2003_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

sans papiers
{: .titre .text-center}

Illegal immigrants are also very often homeless and without media coverage...

Mass media prefer to deal with repression and cops. That's why indy medias
have to cover these subjects, which are supposed to be uninteresting.
This is the aim of this video report, filmed in paris during a demonstration
of illegal immigrants, asking for their situation to be sorted out.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}

[CC by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en){: target="_blank"}
{: .text-center}
