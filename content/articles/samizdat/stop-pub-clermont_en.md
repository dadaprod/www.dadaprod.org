Title: Stop Pub Clermont
Date: 2009-04-18 00:00
Category: samizdat
Keywords: culture jamming, Clermont-Ferrand, dadaprod, samizdat, activism, theora, ogv, Creative Commons, free
Slug: stop-pub-clermont
Lang: en
Summary: Action antipub visant à dénoncer notamment la pollution visuelle...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/stop_pub_clermont_1.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/stop_pub_clermont_1.html){:onClick="ff=window.open('/theme/lecteur/stop_pub_clermont_1.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (26 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/\[dadaprod\].\[samizdat\].stop.pub.Clermont_1_CC.by.sa_2009_512kb.ogv){:download}
[.ogv pro (59 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/%5bdadaprod%5d.%5bsamizdat%5d.stop.pub.Clermont_1_CC.by.sa_2009.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Stop Pub Clermont
{: .titre .text-center}

Culture jamming in Clermont-Ferrand, feb 2009. Culture jamming is the primary
means through which Adbusters fights consumerism in an attempt to return
agency to the individual.Culture jammers attempt to expose the underlying
meaning of an advertisement. By reorganizing media to lend it new meaning,
culture jamming aims to create a large contrast between the corporate image
and the real consequences of corporate behavior. It is a form of protest,
so the culture jammer attempts to be as public as possible in order to stir
up enough trouble to garner the issue real attention.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}

[CC by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.en){: target="_blank"}
{: .text-center}
