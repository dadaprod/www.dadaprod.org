Title: Stop DRM, flashmob
Date: 2006-03-30 00:00
Category: samizdat
Keywords: DADVSI, DRM, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: stop-drm-flashmob
Lang: fr
Summary: La loi DADVSI met en danger les créateurs (de logiciels, artistes..) en limitant entre autres l'utilisation des logiciels d'échanges aux logiciels de travail collaboratif...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/stop-drm.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/stop-drm.html){:onClick="ff=window.open('/theme/lecteur/stop-drm','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (9 Mo)](https://archive.org/download/StopDrmFlashmob/dadaprod.samizdat_flashmob.stop-drm_CC.by.nc-sa_2006_512kb.ogv){:download}
[.mp4 (13 Mo)](https://archive.org/download/StopDrmFlashmob/dadaprod.samizdat.flashmob.stop-drm.CC.by.nc-sa_2006.mp4){:download}
[.ogv (30 Mo)](https://archive.org/download/StopDrmFlashmob/dadaprod.samizdat_flashmob.stop-drm_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Stop DRM, flashmob
{: .titre .text-center}

La loi DADVSI met en danger les créateurs (de logiciels, artistes..) en
limitant entre autres l'utilisation des logiciels d'échanges aux logiciels
de travail collaboratif. Dadaprod refuse ce chantage insufflé par les maisons
de disques, qui non contentes d'avoir tué la création artistique en France
veulent s'approprier toutes les miettes du gâteau !

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
