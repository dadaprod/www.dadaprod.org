Title: Universal, univers sale
Date: 2006-04-22 00:00
Category: samizdat
Keywords: DADVSI, Vivendi, Universal, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: universale
Lang: fr
Summary: Mobilisation contre la loi DADVSI et l'influence néfaste d'Universal...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/universale.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/universale.html){:onClick="ff=window.open('/theme/lecteur/universale','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (64 Mo)](https://archive.org/download/UniversalSale/dadaprod.samizdat_univers.sale_CC.by.nc-sa_2006_512kb.ogv){:download}
[.mp4 (99 Mo)](https://archive.org/download/UniversalSale/dadaprod.samizdat.univers.sale.CC.by.nc-sa_2006.mp4){:download}
[.ogv (107 Mo)](https://archive.org/download/UniversalSale/dadaprod.samizdat_univers.sale_CC.by.nc-sa_2006.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Universal, univers sale
{: .titre .text-center}

Il y a quelques années de cela, dadaprod lançait un appel pour la création
d'une charte **Univers propre**, qui identifierait clairement les labels qui
s'opposent à la politique néfaste des majors (pollution, paupérisation artistique...).
Aussi dadaprod ne pouvait pas manquer de se joindre au collectif stop-drm,
qui manifestait jeudi 20 avril 2006 son opposition à la politique de Vivendi
Universal lors de l'assemblée générale mixte qui avait lieu au carrousel du Louvre.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2006 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
