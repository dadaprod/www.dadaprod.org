Title: souriez vous êtes filmés
Date: 2005-04-15 00:00
Category: samizdat
Keywords: vidéosurveillance, big brother, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: souriez-vous-etes-filmes
Lang: fr
Summary: Dix ans après la légalisation de la vidéosurveillance dans la rue, les caméras de contrôle sont présentes dans tous les domaines de notre vie quotidienne...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/souriez.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/souriez.html){:onClick="ff=window.open('/theme/lecteur/souriez','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (56 Mo)](https://archive.org/download/SouriezVousEtesFilmes/dadaprod.samizdat_souriez.vous.etes.filmes_CC.by.nc-sa_2005_512kb.ogv){:download}
[.mp4 (86 Mo)](https://archive.org/download/SouriezVousEtesFilmes/dadaprod.samizdat.souriez.vous.tes.films.CC.by.nc-sa_2005.mp4){:download}
[.ogv (128 Mo)](https://archive.org/download/SouriezVousEtesFilmes/dadaprod.samizdat_souriez.vous.etes.filmes_CC.by.nc-sa_2005.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

souriez vous êtes filmés
{: .titre .text-center}

Dix ans après la légalisation de la vidéosurveillance dans la rue, les caméras
de contrôle sont présentes dans tous les domaines de notre vie quotidienne...

Dans un contexte sécuritaire et de casse sociale généralisée, cela ne va
pas s’arranger. Au-delà des atteintes fondamentales aux libertés individuelles
et collectives, ces outils de contrôle automatisé symbolisent plus largement
un nouvel univers « souriant » mais aseptisé et normatif, le libéral-totalitarisme.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2005 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
