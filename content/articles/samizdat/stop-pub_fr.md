Title: Stop Pub
Date: 2003-12-19 00:00
Category: samizdat
Keywords: Stop Pub, brazil, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: stop-pub
Lang: fr
Summary: action antipub dans le métro parisien...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/stop_pub.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/stop-pub.html){:onClick="ff=window.open('/theme/lecteur/stop-pub','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (19 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004_512kb.ogv){:download}
[.ogv (44 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004.ogv){:download}
[.ogv pro (192 Mo)](https://archive.org/download/StopPub/dadaprod.samizdat.stop.pub_CC.by.sa_2004_pro.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Stop Pub
{: .titre .text-center}

C’est un reportage enregistré lors de l’action **Stop Pub** du 19 décembre 2003.
Il est accompagné de différents témoignages recueillis lors de même cette action.
La musique est quant à elle composée par le projet [**brazil**]({filename}/articles/artiste/Brazil_fr.md),
qui se sert de ces images pour accompagner ses concerts…

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2003 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
