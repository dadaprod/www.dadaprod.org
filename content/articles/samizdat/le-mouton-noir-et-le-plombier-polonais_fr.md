Title: Le mouton noir et le plombier polonais
Date: 2005-05-01 00:00
Category: samizdat
Keywords: mouton noir, démocratie, référendum, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: le-mouton-noir-et-le-plombier-polonais
Lang: fr
Summary: Mobilisation contre le traité constitutionnel européen...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/mouton_noir.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/mouton_noir.html){:onClick="ff=window.open('/theme/lecteur/mouton_noir','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (44 Mo)](https://archive.org/download/FeteDuNon/dadaprod.samizdat_le.mouton.noir.et.le.plombier.polonais_CC.by.nc-sa_2005_512kb.ogv){:download}
[.mp4 (66 Mo)](https://archive.org/download/FeteDuNon/dadaprod.samizdat.le.mouton.noir.et.le.plombier.polonais.CC.by.nc-sa_2005.mp4){:download}
[.ogv (105 Mo)](https://archive.org/download/FeteDuNon/dadaprod.samizdat_le.mouton.noir.et.le.plombier.polonais_CC.by.nc-sa_2005.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Le mouton noir et le plombier polonais
{: .titre .text-center}

Le mouton noir et le plombier polonais est une fable moderne révélatrice
à bien des égards de l'impasse dans laquelle nous ont menés nos belles
démocraties parlementaires. Quid de la démocratie quand 100% des médias
et 95% des politiciens nous assènent de voter pour un traité gravant dans
le marbre l'ultra libéralisme... Face à une telle propagande, certains
ont décidés de se regrouper dans des comités locaux. Malheureusement, ce
mouvement fut bien vite récupéré par quelques politiciens ayant senti le
vent tourner et espérant ainsi se redorer un nouveau blason social.

Tout ce beau monde, s'est finalement retrouvé une semaine avant le référendum
place de la république à Paris...

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2005 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
