Title: Manif RESF du 31 mars 2012 à Clermont-Fd
Date: 2012-03-31 00:00
Category: samizdat
Keywords: manif, manifestation, RESF, Clermont-Fd, Clermont Ferrand, xénophobie d'Etat, expulsion, droits de l'Homme, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: manif-RESF-31-03-2012
Lang: fr
Summary: Mobilisation face à la régression du droit d'asile...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/RESF63.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/manif-RESF.html){:onClick="ff=window.open('/theme/lecteur/manif-RESF.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv (68 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod_manif-RESF_CC-by-sa-fr_2012.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Manif RESF du 31 mars 2012 à Clermont-Fd
{: .titre .text-center}

Le quinquennat qui se termine aura constitué une aggravation de la stigmatisation
des étrangers vivant en France et particulièrement des demandeurs d'asile
et Sans-Papiers. Avec 33 000 expulsions pour l'année 2011, ce gouvernement
et Claude Guéant le Ministre de l'Intérieur détiennent un bien triste record.

Durant ces cinq années, les droits des étrangers en France et en Europe
n'auront cessé de se réduire. Le Droit d'Asile devient une exception et
l'expulsion la règle : 90 expulsions par jour pour des gens qui ne sont
pas dangereux mais sans cesse en danger.

[...] la suite sur le [site RESF](https://reseau-resf.fr/){: target="_blank"}...

Copyright © 2012 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
