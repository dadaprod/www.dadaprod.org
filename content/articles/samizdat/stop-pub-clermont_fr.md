Title: Stop Pub Clermont
Date: 2009-04-18 00:00
Category: samizdat
Keywords: antipub, Clermont-Ferrand, environnement, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: stop-pub-clermont
Lang: fr
Summary: Action antipub visant à dénoncer notamment la pollution visuelle...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/stop_pub_clermont_1.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/stop_pub_clermont_1.html){:onClick="ff=window.open('/theme/lecteur/stop_pub_clermont_1.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogv 512 kbs (26 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/\[dadaprod\].\[samizdat\].stop.pub.Clermont_1_CC.by.sa_2009_512kb.ogv){:download}
[.ogv pro (59 Mo)](https://download.tuxfamily.org/dadaprod/samizdat/%5bdadaprod%5d.%5bsamizdat%5d.stop.pub.Clermont_1_CC.by.sa_2009.ogv){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Stop Pub Clermont
{: .titre .text-center}

La situation de l'affichage publicitaire est aujourd'hui un scandale dans
notre pays et à Clermont-Ferrand en particulier. Occupant toujours plus
l'espace public, dégradant systématiquement le cadre de vie, il s'agit
d'une véritable pollution visuelle.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa]({static}/images/cc-by-sa_88x31.png)
{: .text-center}
