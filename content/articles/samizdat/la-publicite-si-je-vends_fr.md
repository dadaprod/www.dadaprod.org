Title: La publicité si je vends
Date: 2005-02-11 00:00
Category: samizdat
Keywords: BVP, publicité, publiciDaires, dadaprod, samizdat, gratuit, militant, theora, ogv, Creative Commons, libre
Slug: la-publicite-si-je-vends
Lang: fr
Summary: Action des publiciDaires pour demander la dissolution du BVP...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/samizdat/bvp.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/bvp.html){:onClick="ff=window.open('/theme/lecteur/bvp','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
fichiers [sources](https://archive.org/download/LaPubliciteSiJeVends)
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

La publicité si je vends
{: .titre .text-center}

Le 11 février 2005, répondant à l'appel des **publiciDaires**, une dizaine de
personnes investissent le bureau du BVP (Bureau de Vérification de la Publicité)
situé rue Saint Florentin à Paris, demandant la **dissolution** de cette association
de publicitaires qui essaie de légitimer la publicité en prétendant en réguler
les dérives. Dadaprod dissimulé dans les rangs de cette fronde, filme l'action
et vous propose maintenant le mépris et l'arrogance des publicitaires lorsque leurs cibles se rebiffent...

Le BVP a depuis changé de nom pour devenir ARPP (**A**utorité de **R**égulation
**P**rofessionnelle de la **P**ublicité), pour tenter de masquer la supercherie...

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank"}
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

Copyright © 2005 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by nc sa 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr){: target="_blank"}.

![cc by nc sa]({static}/images/cc-by-nc-sa_88x31.png)
{: .text-center}
