Title: Stop DRM - Michel Sardon
Artist: Michel Sardon
Date: 2006-05-07 00:00
Category: single
Keywords: Michel Sardon, DRM, DIY, Creative Commons, libre
Slug: stop-drm
Lang: en
Summary: In order to protest against the DRM, Banga has comosed a new single...

{!content/include/single/stop-drm_en.md!}

#### Lyrics {: class="text-center"}

Aujourd'hui, je vais vous raconter l'histoire des DRM, comment il sont nés, pourquoi ils ont été crée
Les DRM sont des verrous électroniques, qui vont empêchent de copier, qui vous empêchent de prêter la musique que vous achetez

STOP DRM!! STOP DRM!!

Il était une fois des producteurs de disques, ils veulent que vous payez, ils veulent que vous louez, la musique que vous achetez
Auparavant, vous pouviez faire des compilations pour vos amies, pour votre chérie. Désormais vous serez contrôlés

STOP DRM!! STOP DRM!!

Alors ils ont fait pression sur les élus du peuple, afin qu'ils légifèrent, afin qu'on ne laisse plus faire, de copies privés
Pour la première fois de l'histoire, ils veulent interdire un outil pour son usage, tout doit être maintenant breveté, la musique libre ne doit pas être encadré

STOP DRM!! STOP DRM!!
