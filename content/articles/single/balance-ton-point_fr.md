Title: Balance ton point - Michel Sardon
Artist: Michel Sardon
Date: 2019-12-15 00:00
Category: single
Keywords: Michel Sardon, retraites, DIY, Creative Commons, libre
Slug: balance-ton-point
Lang: fr
Summary: Si une chanson peut aider à mobiliser alors ce détournement musical n'aura pas été totalement inutile. Ce morceau provoquera ou non le rire; En tout cas, c'est sur l'exception de parodie qu'il est construit...

{!content/include/single/balance-ton-point_fr.md!}

#### Les paroles {: class="text-center"}

Ils parlent tous des pensions
qui sont trop hautes, ça fait des trous
Y'a qu'à taxer les patrons
qui rêvent déjà des fonds de pension

Travailler plus, j'suis pas très chaud
Pour gagner moins, c'est pas normal
Et l'âge pivot, j'sais pas ce qui t'faux
Y'a d'jà la décote, tu touches que dalle

Balance ton point
Même si t'es pas dans la rue, je sais qu'au fond t'as compris
Balance ton point
La lutte, c'est classe, tu verras
Balance ton point

Delevoye, laisse-moi te chanter
D'aller te faire en... humhumhumhum
Même si tu mens à la radio
On est pas dupe, démission

Certains se disent que défiler
ça change rien, on perd du blé
Faudrait peut-être pas oublier
que grâce à ça, y'a des retraités

Balance ton point, Balance ton point

Même si le point est à la mode
sur les copies, pour le permis
Ben, tu repasses tout de même le code
Le point ça baisse vite, t'as compris

Balance ton point, Balance ton point

Delevoye, laisse-moi te chanter
D'aller te faire en... humhumhumhum
Même si tu passes à la télé
T'es tout de même un enfoi... humhumhumhum

Dans la rue, il y a le black bloc
qui se fait gazer, amputer
Dans les coulisses, y'a le black rock
qui vient dîner, pour négocier

Macron, laisse-moi te chanter
qu'il faut le reti - rer
Les cadeaux aux riches, c'est terminé
Car, on est mobilisé

Un jour peut-être ça changera
Si tu descends dans la rue
Comme tes aïeux, avant toi
z'ont du courage, je les salue
