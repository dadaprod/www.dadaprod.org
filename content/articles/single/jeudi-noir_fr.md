Title: Jeudi noir - Michel Sardon
Artist: Michel Sardon
Date: 2010-10-24 00:00
Category: single
Keywords: Michel Sardon, logement, squat, DIY, Creative Commons, libre
Slug: jeudi-noir
Lang: fr
Summary: A l'occasion de l'expulsion des squatteurs de la place des Vosges à Paris, dont la cour d'appel venait d'ordonner un jour avant (22 octobre 2010) l'expulsion sans délai, Michel Sardon a décidé de ressortir de son disque dur cette parodie d'un groupe bien connu des années 80...

{!content/include/single/jeudi-noir_fr.md!}

#### Les paroles {: class="text-center"}

Je suis un être à la recherche, d'un appartement à louer
Pas simplement d'une masure, je veux sortir de la précarité

j'en ai assez de ce squat, et de vivre toutes ces galères
ils me disent de rester dans la norme, et de payer pour une misère

Alors je cherche et je trouverai cette apart qui me tente tant
Alors je cherche et je trouverai cette apart qui me tente tant
Qui me tente tant han han

Particulier à particulière cherche particulière à particulier
Une fiche de paie, un garant blindé et une bonne dose de savoir faire , Savoir faire

Vous comprendrez que de tels projets, parfois sont difficiles à réaliser
Ils sont au'tour de moi si fragiles, ce n'est pas parmi eux que je trouverai

Je dois trouver de nouveaux horizons Mais je finis parfois par tourner en rond

Alors je cherche et je trouverai cette apart qui me tente tant
Alors je cherche et je trouverai cette apart qui me tente tant
Qui me tente tant han han

Particulier à particulière cherche particulière à particulier
Une fiche de paie, un garant et une bonne dose de savoir faire , Savoir faire
