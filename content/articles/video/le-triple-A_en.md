Title: Le Triple A (AAA) - Michel Sardon
Artist: Michel Sardon
Date: 2012-02-05 00:00
Category: video
Keywords: Michel Sardon, dette, AAA, DIY, Creative Commons, libre
Slug: le-triple-A
Lang: en
Summary: Here's a video montage based on various Web sources and, of course, the song Triple A (AAA)...

{!content/include/video/le-triple-A_en.md!}

#### Lyrics {: class="text-center"}

La dette est un plat, qui se prépare à l’avance
pour avoir de la saveur, et éviter les dépenses
pour plaire aux plus riches, et ruiner la France
Ah Ah, le Triple A

En France cela fait, environ trente années
que la recette, a été pensée
C'est la concurrence, libre, et non faussée
Ah Ah, le Triple A

Avant de mettre au chaud, il faut la liberté
de circulation, des capitaux
C'est la concurrence, libre, et non faussée
Ah Ah, le Triple A

Les ingrédients prêts, il faut baisser
les impôts des riches, et interdire
la création monétaire, par un État
Ah Ah, le Triple A

Dans un même temps, il faut augmenter
les plus hauts salaires, autour de 30%
Tout en limitant à 3%
la hausse des 90% restant

Ainsi la part des salaires, dans la sauce, « PIB »
diminue au profit des dividendes, aux actionnaires
L’assaisonnement, est presque parfait
entre 8 et 9%, de transfert

Mais les plus riches peuvent encore, s’enrichir
En prêtant à l’État, en toute, sécurité
C'est la garantie, d'un bon taux, d’intérêt
Ah Ah, le Triple A

Si la cuisson s’est bien déroulée
la part des recettes a enfin chuté
La part des dépenses, elle, s'est stabilisée
Sortie du four, le déficit, a gonflé

C’est le signe, votre dette est prête !
Pour mettre en appétit, les conseils du chef
un zeste de culpabilisation, pour nos enfants
Ah Ah, le Triple A

Assaisonner, d’une cuillerée
de suppression des droits, et de nos acquis
Nous vivons au, dessus de nos moyens
Ah Ah, le Triple A

Pour une meilleure, digestion
pour les peuples, un verre de division
On ne va pas payer, pour ces fainéants !
Ah Ah, le Triple A
