Title: Brazil
Artist: Brazil
Date: 2001-09-14 00:00
Category: artiste
Keywords: Michel Sardon, Terry Gillian, Brazil, work, musique, post-rock, DIY, Creative Commons, libre
Slug: Brazil
Lang: en
Summary: Inspired by Terry Gillian's film, brazil's music oscillates between the moods of everyday life and sonic heartbreak...


<div class="btn-group">
<button type="button" class="btn btn-groupes btn-sm" onclick="change(1);">introduction</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(2);">audio Cd</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(8);">singles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(9);">Video clip</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(3);">lyrics</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(4);">press</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(7);">agenda</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(5);">bio</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(6);">data sheets</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(10);">tags</button>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche1" class="presentation" markdown="1">
**Brazil** is one of the side project of **Michel Sardon**.

The music of Brazil is inspired by post-rock...
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche2" class="presentation" markdown="1">
Below, you'll find all of Brazil's contributions to the Dadaprod's discs,
which you can also find in the [dedicated section]({category}album) of
the web site ou or directly by clicking on the following links :
[A Bas Travailler (live)]({filename}/articles/album/A-Bas-Travailler_en.md),
[La faim du travail]({filename}/articles/album/la-faim-du-travail_en.md),
[Premières nouvelles]({filename}/articles/album/premieres-nouvelles_en.md).

{!content/include/album/A-Bas-Travailler_en.md!}
{!content/include/album/la-faim-du-travail_en.md!}
{!content/include/album/premieres-nouvelles_en.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche8" class="presentation" markdown="1">
{!content/include/single/terroriste_en.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche9" class="presentation" markdown="1">
{!content/include/video/A-bas-travailler_en.md!}
</div>
</div>

<div class="pres_ntation">
<div id="couche3" class="presentation">
<script type="text/javascript">
    let paroles =  [
        "On voudrait nous faire croire que l\'homme est mort\r\n                         que l\'on est des victimes, un animal comme un autre,\r\n                           qu\'il faut savoir bouffer son prochain pour survivre</BR>\r\nJe travaille en banlieue ouest, bien payé pour ce que je fais devrais-je me satisfaire? Rentrer dans les rangs, suivre la règle et me taire?</BR>\r\nMais je n\'y arrive pas, tous les matins je me lève et c\'est la même angoisse...\r\nAutour de moi, tout le monde semble persuadé que je ne suis pas normal:\r\nrebelles, réactionnaires et anarchistes se mélangent à mon train</BR>\r\nFaut-il que je me fasse interner? Suis-je un danger pour mes contemporains?\r\nPourquoi ne suis-je pas normal?</BR>\r\nL\'école nous apprend la discipline, alors qu\'elle devrait nous enseigner l\'éveil.\r\nL\'histoire est incertaine, nos sociétés contemporaines sont-elles vraiment plus heureuses que celles qui périssaient naguère?</BR>\r\nTravaille bien à l\'école pour avoir un bon métier</BR>\r\nTravaille bien à l\'école et apprend à gagner\r\n\r\n<P>Vous vous levez tous les matins, à sept heures. Puis vous prenez le train ou la voiture pour vous rendre au travail. Par la fenêtre, vous apercevez les visages blafards de vos contemporains.\r\nVous vivez parmi eux, vous êtes eux. </BR>\r\nNéanmoins votre ego ne supporte plus cette image que l\'on vous renvoie, celle d\'un petit cadre moyen qui s\'accroche à ses maigres degrés de liberté: le choix du supermarché et la marque du Dvd. </BR>\r\nVous admettez que la vie en régime libéral n\'est pas parfaite, qu\'il faut savoir feinter, se troubler et se battre. Néanmoins, elle représente pour vous la seule idéologie envisageable. Et après tout, n\'est-elle pas la plus proche du règne animal?</BR>\r\nAlors il ne reste plus que la jachère. Brûler, brûler à en crever! De toute façon, il n\'y a plus rien à gagner, plus rien à perdre.</BR>\r\nNon, non à l\'Etat et à ses politiques véreux, non à la e-economie et à sa bourse folle. Le marché nous asphyxie, l\'état est complice, il n\'y a plus rien à gagner, plus rien à perdre... Il faut tout brûler, tout brûler</P>",
        "<P>\r\nJe ne travaille pas, tu ne travailles pas\r\nOn se lève tous les matins et on fait rien\r\ntu ne travailles pas, Je ne travaille pas\r\n</BR>\r\nTous les deux chômeurs de longues durées, on était fait pour se rencontrer\r\nJe ne travaille pas, tu ne travailles pas\r\n</P>\r\n<P>\r\nHarceler et sous payer, on n\'va pas se lever pour se faire exploiter\r\nJe ne travaille pas, tu ne travailles pas\r\n</BR>\r\nA quoi bon travailler, à quoi bon se défoncer\r\nOn finira par se faire licencier, tu ne travailles pas</BR>\r\nAlors on passe toutes nos journées, à lire et glander devant la télé\r\n</P>\r\n<P><I>\r\nla fin du travail, la faim du travail?\r\n</BR>\r\nla fin du travail, la faim du travail?\r\n</I></P>",
        "<P>A 16 ans, à 16 ans, j\'avais des boutons sur la gueule \r\npartout, alors les filles../</BR>\r\nMais ça l'a quand même fait avec une... dont je ne me rappelle plus le nom\r\nElle était blonde, je crois?</P>\r\n\r\n<P>A 17 ans, à 17 ans j\'étais tout seul... </BR>\r\nAh non il y a eu Jennifer, mais je ne suis pas sûr, \r\nnon je ne suis pas sûr que ce soit à 17 ans</P>\r\n\r\n<P>A 18 ans, à 18 ans j\'ai eu mon bac, \r\nseul comme toujours...</BR>\r\nJ\'ai dû être heureux, enfin j\'imagine, \r\nmais ca n\'allait pas très fort quand même</P>\r\n\r\n<P><I>transparent à la vie, je traverse les villes</BR>\r\ntransparent à la vie, je traverse les villes</I></P>\r\n\r\n<P>A 19 ans, à 19 ans je suis devenu étudiant, \r\net c\'était encore plus dur, encore plus dur, parce que j\'avais encore moins de temps</BR>\r\nIl a fallu attendre les vacances pour qu\'il se passe enfin quelque chose\r\nj\'étais amoureux d\'elle, mais elle a pas voulu de moi\r\nelle a pas voulu de moi, elle a préféré sortir avec un de mes copains, \r\nils ne se sont jamais revu je crois..</P>\r\n\r\n<P>A 20 ans, à 20 ans, j\'ai déclaré ma flamme \r\nà une fille alors que je ne l\'avais vu que pendant quelques heures, en état d\'ivresse... </BR>\r\nElle me l\'a signalé dans un courrier; C\'était un peu ridicule, \r\nde demander de l\'amour à quelqu\'un, seulement après quelques verres échangés</P>\r\n\r\n<P>A 21 ans, à 21 ans ça pas été facile, \r\nle doute m\'habitait, je n\'arrivai plus à travailler</BR>\r\nmais j\'ai quand même eu mes concours et ça été la promesse de plus beaux jours</BR>\r\n\r\n<P><I>transparent à la vie, je traverse les villes</BR>\r\ntransparent à la vie, je traverse les villes</I></P>\r\n\r\n<P>A 22 ans, à 22 ans je suis tombé amoureux d\'une étudiante...\r\nje lui avais fait une cassette de mes chansons favorites, \r\nmais elle a pas vraiment compris, elle a pas vraiment compris\r\nenfin si, à la fin quand je lui ai envoyé la lettre</P>\r\n\r\n<P>A 23 ans, à 23 ans j\'ai commencé à jouer de la guitare \r\net même si ce n\'était pas conscient avec le recul, \r\navec le recul, je me dis que j\'avais certainement dû vouloir exorciser toute cette peine</P>\r\n\r\n<P>a 24 ans, à 24 ans je me suis exilé en Angleterre, \r\nj\'ai continué à écumer les pubs, les soirées\r\nmais ça n\'a pas plus marché, ca n\'a pas plus marché\r\nmon âme sœur ne devait certainement pas être anglaise</P>\r\n\r\n<P>Et puis, on s\'est rencontré rue didot, à Paris\r\ndans ce foyer où l\'on était venu pour notre premier boulot</BR>\r\nça fait déjà trois ans maintenant et parfois ça fait un peu peur, \r\ncette prison sans mur</BR>\r\nmais je sais qu\'au moins maintenant, on sera plus jamais seul\r\net même si c\'est pas facile tous les jours, à deux on est plus fort\r\non se soutient, il nous arrivera rien, j\'ai confiance</BR>\r\net même si je ne suis pas à la hauteur tous les jours et que parfois je dévisage d\'autres filles, \r\navec le boulot, la lessive, la vaisselle,\r\ntoutes ces petites contingences qui rendent nos vies trop matérielles</BR>\r\nil nous faudra encore chercher, trouver d\'autres solutions\r\nprendre sur nous, se toucher, s\'écouter se parler et apprendre \r\nespérons seulement que ce soit dans un monde meilleur</P>",
        "<P>Je vis dans votre ville, mais vous ne me connaissez pas. Partout où vous allez, je vous accompagne, je suis là. Je suis un témoignage, mais vous ne me reconnaissez pas.</P>\r\n\r\n<P>Nous parcourons des vallées, nous traversons des villes, des visages hagards longent le bord des routes <font face=\"arial\" size=\"2\">Marion for ever, Ni Dieu ni maître, A bas le F.N</font>\r\nAu loin une carcasse de voiture consume ses derniers restes <font face=\"arial\" size=\"2\">Sophie je t’aime</font></BR>\r\nNous pénétrons l’intérieur de cette bête immonde <font face=\"arial\" size=\"2\">U.S go home</font>\r\nLes trottoirs dégueulent leur misère, toutes ces vies gâchées d’avance\r\n<font face=\"arial\" size=\"2\">Ecrivez partout !</font>\r\nDes prostituées au regard sombre se jettent sous nos roues <font face=\"arial\" size=\"2\">06 36 93 04 88, téléphonez-moi</font> Quand tu l’as rencontrée, tout de suite tu as su qu’elle représentait ta dernière chance de bonheur <font face=\"arial\" size=\"2\">Elle et lui, amants pour la vie</font></P>\r\n\r\n<P><font face=\"arial\" size=\"2\">Metalheadz, black turttle, la société est une fleur carnivore, Pixies, AC DC, Stéphane + Odile = love, mort aux vaches, black power, nicke ta mère et nicke la police, Nadine tu pues, allez l’O.M, Camel, Rachid, Thierry, Olive, La cole, bebel, Tidou, moumou, black skunk ! Famille je vous hais !</font></P>\r\n\r\nJe suis un graffiti et je représente le dernier signe de vie\r\nJe suis un graffiti, le dernier signe de vie\r\n\r\n<P>Les marchés contrôlent nos vies, <font face=\"arial\" size=\"2\">vous n’imaginez pas tout ce que Citröen peut faire</font>. Les idéologies d’antan ont été peu à peu remplacées par des messages publicitaires : <font face=\"arial\" size=\"2\">bienvenue dans la vie.com</BR>\r\nJust Do It </font>faisant désormais référence en matière de libre arbitre pour nos petits chérubins. <font face=\"arial\" size=\"2\">J’en ai rêvé, Sony l’a fait</font>. Comble de cette invasion mercantile, la publicité a investi le champ de l’éducation. <font face=\"arial\" size=\"2\">E.D.F nous vous devons bien plus que la lumière</font>. Désormais, on apprendra les couleurs avec des marques : <font face=\"arial\" size=\"2\">enjoy coca cola</font>.</BR> Des panneaux publicitaires aux émissions de télévison, le marketing dirige nos vies. Devant une telle connivence et afin de réduire nos concitoyens à de simples consommateurs, le cynisme est de rigueur.</P>\r\n\r\n<font face=\"arial\" size=\"2\">Carte noir, un café nommé désir. Société générale, conjuguons nos talents. Sega, c’est plus fort que toi. S.F.R, le monde sans fil. Nokia, connecting people. L’Oréal, parce que je le vaux bien. La poste, on a tous a y gagner. Adecco, ca ne change pas le monde, mais ça y contribue. Mobalpa, on est là pour ça. Charles Gervais, il est odieux, mais c’est divin. Nestlé, c’est fort en chocolat. Rhone Poulenc, bienvenu dans un monde meilleur.</BR>\r\nbienvenu dans un monde meilleur</font>",
        "<P>\r\nC\'était un samedi après-midi comme les autres, où on s\'était réuni avec quelques vieux amis.</BR>\r\nil y avait là Paul, Matt, Teuf et quelques autres de passage</P>\r\n<P>\r\nEnsemble, on est allé au supermarché pour acheter des bières, les pizzas, on les commanderait un peu plus tard</BR>\r\nPuis on a commencé à boire et à écouter des disques, fumer des pétards et raconter quelques vieilles plaisanteries</BR>\r\nMais quelque chose avait changé, il n\'y avait plus rien de drôle à nos vies,\r\nelles semblaient désormais si fades</P>\r\n<P>\r\nC\'était un samedi après-midi où on s\'était réunis</BR>\r\nmais il n\'a plus rien à faire, il faut tourner la page, oublier le temps de notre adolescence révolue et apprendre à accepter nos vies, nos toutes petites vies</BR>\r\nComprendre pourquoi nous avons perdu cette envie, de communiquer les uns aux autres, nos passions, nos amours,\r\ncomment on allait tout changer</BR>la marche triomphale et l\'avènement du grand soir...</P>\r\n<P>\r\nLà-dessus est arrivé Chris, la femme de Tom. Elle a commencé à gueuler \r\nparce qu\'on avait tout cradé. </BR>\r\nAlors on s\'est fait virer et on s\'est tous retrouvé à la terrasse du troquet d\'à côté\r\net on a essayé d\'analyser, comment on avait pu en arriver là</BR>\r\nà nous réunir comme ça, à boire des bières et écouter des disques, \r\nsans trop savoir pourquoi</P>"
    ]
</script>
<select name="txtid" onChange="parole(this.value, paroles);" style="border-color: #000000; background-color: #ffffff;">
    <option value="">read the lyrics...</option>
    <option value="">________________</option>
    <option value="">la faim du travail ></option>
    <option value="">---------</option>
    <option value="0">Paul, véro, Sven...</option>
    <option value="1">la fin du travail</option>
    <option value="2">une vie à deux</option>
    <option value="3">graffiti</option>
    <option value="4">un samedi après midi...</option>
    <option value="">________________</option>
    <option value="">Premières nouvelles ></option>
    <option value="">---------</option>
    <option value="0">Paul, véro, Sven...</option>
    <option value="4">un samedi après midi...</option>
</select>
<div id="divParole">
</div>
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche4" class="presentation" markdown="1">

### Cd: la faim du travail
#### Etherreal
*on peut reconnaître à Brazil un talent indéniable dans les compositions musicales, dans la variété des ambiances et la façon dont elles sont amenées puisqu'à aucun moment la lassitude ne se fait sentir à l'écoute de ce disque, qu'il est vivement conseillé d'écouter en travaillant !!*
voir l'article complet
#### Jade Web
*Le luxe suprême de l’homme moderne est l’anonymat et l’isolement voulus. Voilà une des leçons que Terry Gillian nous aura apprise.*
*Brazil creuse d’une certaine manière dans ce sens en donnant une connotation hermétique à sa petite œuvre (plaisir solitaire ?), où s’exposent ici et là, extraits de bandes originales et de films choisis, de l’enfant nue de Pialat aux 400 coups de Truffaut, Trust me de Hartley et 300 appels par jour de Christophe d’Havilliée, avec en arrière plan, la musique sombre de rues désertes et les éclairs de joie des lampadaires.*
voir l'article complet
#### Liability
*La montée musicale aussi diffère. On sent un penchant pour Godspeed, avec la batterie et le violon qui raillent la guitare, et toujours un piano pour calmer la mise.*
*Mais voilà, il n'y a pas que de l'acharnement dans ce disque…On y trouve aussi de délicieuses parades acoustiques ("La pause café", "L'herbe rouge") où le minimalisme n'est autre chose que l'instinct, à sa source, de l'auteur. Un véritable auteur. La prétention et la maison de disque en moins, celui-ci est pourtant profus d'excellentes idées.*

### Cd: Premières nouvelles
#### Magic!
*Brazil, décline instruments et Cubase en clairs de lune équatoriaux et cités de béton. D'une voix blanche, Stéphane, déclame un pamphlet anti-success story, derrière lequel guitare, piano et violon accélèrent, prennent en pleine poitrine le souffle d'un RER qui frôle à toute vitesse les cols blancs sur un quai de banlieu.*
[voir l'article complet](theme/chroniques/magic_premier_1.html){. target="_blank"}

</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche7" class="presentation" markdown="1">
### They belong to the past...
- Paris (20h30 - en soutien aux 62 inculpés, )
- Paris (20h - festival Liability, )
- Ferme des Costils - Cambremer (19h - vernissage la ferme et vous, )
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche5" class="presentation" markdown="1">
It was in the heart of his native Normandy, still in his teens, that the
young **Michel Sardon** was moved by the existence of a hitherto unknown music.
It was while reading an issue of Best covering the umpteenth comeback of
Pink Floyd that the young Sardon discovered the existence of the Pixies.
The local media library soon became the scene of encounters with vinyl records.
It was only a few years later that Michel decided to defy the family consensus
that music was a domain reserved for others. And so he launched into his
first chords. The strumming of strings quickly became creative, exorcising
the unfathomable evil that had gnawed at him since his earliest childhood.
Accompanied by a handful of friends, his compositions followed on from his stays
in Normandy and England, ending up in Paris, where Michel launched into M.A.O and D.I.Y!

In 2001, Michel launched **Dadaprod**, seeking to create a new non-commercial
space where music could be exchanged over the Internet or via CD-Rs.
In total opposition to the behemoths of the record industry, dadaprod
castigates the gravediggers of creation, calling for free music where artists
would no longer be bank accounts belonging to the jet-set or elected by TV shows.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche6" class="presentation" markdown="1">
* [download live technical sheet](theme/live/fiche_technique_fr.pdf){. target="_blank"}
* [download stage map](theme/live/plan_scene_fr.pdf){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche10" class="presentation" markdown="1">
### Michel Sardon, Terry Gillian, **Brazil**, work, musique, **post-rock**, DIY, Creative Commons, libre
</div>
</div>
