Title: Banga
Artist: Banga
Date: 2001-09-14 00:00
Category: artiste
Keywords: Banga, musique, pop, folk-rock, DIY, Creative Commons, libre
Slug: Banga
Lang: fr
Summary: Pop boisée aux accents de Low Barlow ou des Modern Lovers...


<div class="btn-group">
<button type="button" class="btn btn-groupes btn-sm" onclick="change(1);">présentation</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(2);">audio Cd</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(8);">singles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(9);">Video clip</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(3);">paroles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(4);">presse</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(7);">agenda</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(5);">bio</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(6);">fiches techniques</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(10);">tags</button>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche1" class="presentation" markdown="1">
Banga est un des projets de **Michel Sardon** (à coté de Brazil plus orienté vers le post-rock, de Malaussene...).

Parti sur une pop boisée aux accents de Low Barlow ou des Modern Lovers,
la musique de Banga s'est peu à peu étoffé, affirmant désormais un son plus rock...
Les ambiances sonores explorent ainsi le folk-rock, punk-folk.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche2" class="presentation" markdown="1">
Retrouvez ci-dessous les participations de Banga aux disques de Dadaprod,
que vous trouverez également via la [section dédiée]({category}album) sur
le site ou directement via les liens suivants :
[Ivresse de la paresse]({filename}/articles/album/ivresse-de-la-paresse_fr.md),
[Premières nouvelles]({filename}/articles/album/premieres-nouvelles_fr.md).

{!content/include/album/ivresse-de-la-paresse_fr.md!}
{!content/include/album/premieres-nouvelles_fr.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche8" class="presentation" markdown="1">
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche9" class="presentation" markdown="1">
</div>
</div>

<div class="pres_ntation">
<div id="couche3" class="presentation">
<script type="text/javascript">
    let paroles =  [
        "<P>\r\nCome on now, hear my preach! Come on, come with them</BR>\r\nthe world is rockin so badly\r\nI can feel such a pain</P>\r\n<P>\r\nmy mates and I standing up so sadly,\r\nwaiting for a better world</P>\r\n<P><I>\r\nWe are the travelers of the word united on a sunday afternoon</I></P>\r\n<P>\r\nWake up now, join our lines\r\nWake up now, before it\'s too late</BR>\r\nthe world is dying irremediably,\r\nI can feel such a pain</P>\r\n<P>\r\nbusiness men are ruling the world,\r\nselling it, you should have seen them</P>\r\n<P><I>\r\nWe are the travelers of the word united on a sunday afternoon</I></P>",
        "<P>I am writing in my diairy, I love you in my dreams</BR>\r\nYou\'re the kind of girl I really like, we will meet one day</P>\r\n<P><I>\r\nLove me deep and love me tender, love me true</BR>\r\nI just can\'t get back to you</I></P>\r\n\r\n<P>I met you at the librairy, all the dreams (that) we share</BR>\r\nfor you I am really care, you are my one true love</P>\r\n\r\n<P><I>Love me deep and love me tender, love me true</BR>\r\nI just can\'t get back to you</I></P>",
        "<P>You treat me so bad, so stab me in the back</BR>\r\nyou know you let me so low, you were the only girl I ever loved</P>\r\n<P>\r\nYou have seen my badest things, I\'ve got yo see you badest things</P>\r\n<P>\r\nI say I dont want to hurt you no more, No I don\'t want to see you no more</P>\r\n<P>\r\nYou have seen my badest things, I\'ve got yo see you badest things</P>\r\n<P></BR>\r\nNow, I\'m counting my days; I\'ve got to remember the time</BR>\r\nYou hurt me so bad, You were my one true love</P>\r\n<P>\r\nYou have seen my badest things, I\'ve got yo see you badest things</P>\r\n<P>\r\nI say I dont want to hurt you no more, No I don\'t want to see you no more</P>",
        "<P>Assis a la fenêtre je vois, l\'évidence\r\nmais qu\'est-ce qui nous arrive enfin, ces silences</BR>\r\nAprès toutes ces années, notre vie se déchire\r\nje ne me rappelle qu\'à peine de la forme de ton sourire</P>\r\n<P><I>\r\nMais je ne comprends pas bien, ce que je fais là</BR>\r\nmon amour, je t\'aime et toi</I></P>\r\n<P>\r\nmais ne me regarde pas comme ça mon amour</BR>\r\nOn aura fait ce que l\'on aura pu, tous les jours</BR>\r\nOn ne se parle plus, on ne se touche plus, on s\'ignore</BR>\r\nalors quoi bon continuer cette histoire\r\n</P><P><I>\r\nMais je ne comprends pas bien, ce que je fais là</BR>\r\nmon amour, je t\'aime et toi</I></P>\r\n<P>\r\nPuisqu\'entre nous, c\'est même plus la peine de se déchirer, autant se quitter</P>"
    ]
</script>
<select name="txtid" onChange="parole(this.value, paroles);" style="border-color: #000000; background-color: #ffffff;">
    <option value="">lire les textes...</option>
    <option value="">________________</option>
    <option value="">ivresse de la paresse ></option>
    <option value="">---------</option>
    <option value="0">travellers united</option>
    <option value="1">love</option>
    <option value="2">u treat me so bad</option>
    <option value="">________________</option>
    <option value="">Premières nouvelles ></option>
    <option value="">---------</option>
    <option value="3">la rupture</option>
</select>
<div id="divParole">
</div>
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche4" class="presentation" markdown="1">
### Cd: ivresse de la paresse
#### tatapoum
*De la pop tranquille à sa copine plus énervée (hommage au prochain concert des Pixies ?), tour d'horizon personalisé de ce que l'on aime y trouver. Cette boisson là a des bulles en plus et se boit très frais : ne serait-ce pas plutôt Orangina.*
#### Longueur d'Ondes
[...] *Puis Banga enchaîne avec une pop boisée aux accents Modern Lovers.*
[voir l'article complet](theme/chroniques/dada_003_ondes.html){. target="_blank"}
#### A découvrir absolument
*Banga c'est la meilleure rencontre que nous ayons fait depuis des lustres, la rencontre d'un type avec autant de savoir-faire que vous et moi, autant de possibilité au chant, mais avec une plume incroyable qui devrait plaire autant au fille que de nous faire groover.*

### Cd: Premières nouvelles
#### Magic!
*Banga, le voit gratter une guitare et aligner les ballades quelque peu misérabilistes. On est pas loin de la bonne humeur de serail killer de Daniel Johnston, ainsi que de la sensibilité écorchée de Smog, mais la naïveté affichée des morceaux signe un personnage en décalage.*
[voir l'article complet](theme/chroniques/magic_premier_2.html){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche7" class="presentation" markdown="1">
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche5" class="presentation" markdown="1">
C’est au cœur de sa Normandie natale, encore en pleine adolescence, que le
jeune **Michel Sardon** s’émeut de l’existence d’une musique jusqu’alors inconnue.
C'est à la lecture d’un numéro de Best couvrant un énième come-back du groupe
Pink Floyd que le jeune sardon découvre l'existence des Pixies.
La médiathèque municipale de la ville voisine, devient alors bien vite le
théâtre de rencontres avec des vinyles, puis des Cds estampillés "musique pas comme les autres",
comme autant de remparts face à la coercition du milieu éducatif et familiale.
Et ce n’est que quelques années plus tard que Michel, décide de braver le
consensus familiale selon lequel la musique serait un domaine réservé aux autres.
Il se lance alors dans ses premiers accords. Le frôlement des cordes devient
rapidement créatif, exorcisant ainsi ce mal insondable qui le ronge depuis sa
plus tendre enfance. Accompagné par quelques amis, les compositions se succèdent
au gré de ses séjours en Normandie et en Angleterre, pour finir à Paris où Michel
se lance dans la M.A.O et le D.I.Y !

Sur sa lancée, Michel lance en 2001 **Dadaprod**, cherchant à créer un nouvel
espace non marchand où la musique s’échange sur Internet ou via des Cd-R.
En totale opposition par rapport aux mastodontes de l’industrie du disque,
dadaprod fustige les fossoyeurs de la création, revendiquant une musique
libre où les artistes ne seraient plus des comptes en banque appartenant
à la jet-set où des élus dans des programmes autant racoleurs que mercantiles.
Michel décide alors de décliner sa sensibilité musicale au gré de plusieurs projets solos,
comme banga (créant ainsi le mouvement punk-folk) et brazil, plus ouvert à la musique instrumentale.

</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche6" class="presentation" markdown="1">
* [Téléchargez la fiche technique](theme/live/fiche_technique_fr.pdf){. target="_blank"}
* [Téléchargez le plan de scène](theme/live/plan_scene_fr.pdf){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche10" class="presentation" markdown="1">
### Banga, musique, pop, folk-rock, DIY, Creative Commons, libre
</div>
</div>
