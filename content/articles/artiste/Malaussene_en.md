Title: Malaussene
Artist: Malaussene
Date: 2003-03-08 00:00
Category: artiste
Keywords: Malaussene, music, pop, DIY, Creative Commons, free
Slug: Malaussene
Lang: en
Summary: post-pop with subtle and varied accents....


<div class="btn-group">
<button type="button" class="btn btn-groupes btn-sm" onclick="change(1);">introduction</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(2);">audio Cd</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(8);">singles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(9);">Video clip</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(3);">lyrics</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(4);">press</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(7);">agenda</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(5);">bio</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(6);">data sheets</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(10);">tags</button>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche1" class="presentation" markdown="1">
**Malaussene** is one of the side project of **Michel Sardon**.

This project was created to fit to pop songs that were a bit different to others songs of Banga...
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche2" class="presentation" markdown="1">
Below, you'll find all of Malaussene's contributions to the Dadaprod's discs,
which you can also find in the [dedicated section]({category}album) of
the web site ou or directly by clicking on the following link :
[ivresse de la paresse]({filename}/articles/album/ivresse-de-la-paresse_en.md).

{!content/include/album/ivresse-de-la-paresse_en.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche8" class="presentation" markdown="1">
{!content/include/single/dobranoc_en.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche9" class="presentation" markdown="1">
</div>
</div>

<div class="pres_ntation">
<div id="couche3" class="presentation">
<script type="text/javascript">
    let paroles =  [
        "<P>Après toutes ces révolutions manquées; Vendues à des marchands peu scrupuleux. Ils préfèrent rester chez eux et regarder Laurent Boyer</BR>\r\nDes journaux télévisés au téléthon, ils s’apitoient devant leur télévision. Mais quand un SDF leur fait la manche, ils s’enfuient dans l’indifférence</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>De Libération à Sarkozy, les français ont voté ce qu’on leur avait dit. Et quand vient le temps du mea-culpa, ce sont tous les rois du télé achat</BR>\r\n\r\nLe premier mai, ils défilent : ils ont honte d’être français. Le lendemain, ils se défilent en parlant ciné devant un café</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>Et quand vient le temps des lois sécuritaires, on sabre le champagne sur France Inter. Qu’il soit de gauche ou de droite, ce sont toujours les mêmes costard-cravates</BR>\r\n\r\nQui s’octroient le droit de décider : de la vie et de la mort de milliers d’innocents. Et ce n’est qu’avec guère plus de sentiments, qu’ils sacrifient le bien public aux intérêts privés</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>Et quand ils dénoncent la consommation, ce n’est qu’une autre conversation. Ce sont des nostalgiques du grand soir, qui regardent leur nombril devant un miroir</BR>\r\nPuis quand arrive Noël l’opportuniste. Ils s’affairent pour leur grande fête consumériste. Ils s’enorgueillissent d’offrir à leurs descendants, le fruit du travail d’autres enfants</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi",
        "<P>Je suis loin de toi, dans ces villes\r\nOù je t’aperçois, au fond de ruelles fertiles</BR>\r\nPlus je m’éloigne de toi, et plus je pense à toi. Je me taris de ton absence</P>\r\n\r\n<P>La foule, autour de moi, murmure ton nom, dans des langues que je comprends pas. Quand je pense à ces fesses, que personne ne caresse. J’imagine ta bouche, parcourant le mien\r\n</BR>\r\nCe ne sera pas la première et ce ne sera pas la dernière fois, que je m’éloigne de toi</P>\r\n\r\nMais je voudrais encore une fois te serrer dans mes bras\r\nOh quand je pense à ces fesses que personne ne caresse\r\n\r\n<P>Plus je m’éloigne de toi, et plus je pense à toi\r\nJe me taris de ton absence</P>",
        "<P>Oh songe des nuits, qui sont pour moi des rêves éveillés. Et qui tout comme la pluie, ils s’arrêtent dès qu’ils sont éclairés</BR>\r\nHier je t’ai reconnue. Telle une apparition, tu as disparu. Depuis tu hantes mes nuits, je ne veux que te revoir. Car je sais qu’à minuit, tu reviendras dans le noir</P>\r\n\r\nReste encore auprès de moi, l’aurore se tord elle ne viendra pas\r\n\r\n<P>Oh ma douce de nuit d’amour. Je t’aimerai pour toujours. Je rejoindrai ton royaume secret et on s’aimera pour l’éternité</BR>\r\nMais l’aurore trouble ma vue. Où es-tu ? Je ne te vois plus</P>\r\n\r\nReste encore auprès de moi, l’aurore se tord elle ne viendra pas",
        "<P>J’erre dans les villes, les murs gris me renvoient leur indifférence. Je m’abîme dans ce puits sans fond, m’enfonçant aux hasards des ruelles sombres</P>\r\n\r\nLes bruits de la ville se muent dans un silence abyssal\r\nTout me semble froid et figé, mais il ne faut plus, y penser\r\n\r\n<P>Mais quand vient le soir, je monte dans un bus, le mouvement m’enivre. Recréant la vie, le soleil rougi me réchauffe le cœur</P>\r\n\r\nLes bruits de la ville se muent dans un silence abyssal\r\nTout me semble froid et figé, mais il ne faut plus, y penser",
        "<P>C’est l’histoire d’un con, qu’a jamais dit non. Il travaille du matin au soir, où il broie du noir. Oh un petit noir, au petit matin, histoire de faire passer, cette sale journée</P>\r\n\r\n<P>Cette histoire de con, c’est l’histoire de Gaston; Qu’il soit travailleur, ou commercial manager. Il travaille pour se payer, une machine à laver, la dernière Peugeot, oui c’est un Corniaud !</P>\r\n\r\n<P>Un jour Gaston s’arrête, d’être un social traite. Il ne veut plus travailler, ni se faire exploiter. Il pourra plus se payer de lecteur Dvd; Ses voisins de palier, le traite de PD</P>\r\n\r\n<P>Ces homo Videndis ne sont pas des artistes, qui bossent à la télé, ou dans les cinés. Ce sont des gastons, comme vous et moi, alors ne vous laissez surtout pas: </BR>\r\nêtre pris pour un con…</P>",
        "<P>La France a peur aujourd’hui\r\nTout ça c’est Sarko qui l’a dit. Que t’habites en province, que t’habites à Paris. T’oses plus sortir après minuit</P>\r\n\r\nLa France a peur aujourd’hui\r\n\r\n<P>Le 21 avril, les médias sont surpris. Les Français ont voté nazi. Mais comme le mea-culpa, ils connaissent pas. C’est pas de la faute à ces cons-là</P>\r\n\r\nLa France a le F.N aujourd’hui\r\n\r\n<P>Désormais nous sommes tous des terroristes; Surtout les sans-papiers, les activistes. Sur internet, à la télé, on vous épie, même dans la rue, vous êtes suivis</P>\r\n\r\nSouriez, vous êtes filmés\r\n\r\n<P>J’ai pas choisi de mourir ici\r\nFrance pays des droits de l’homme, pays de Sodome, j’ai pas choisi (bis)\r\n</P>",
        "<P>First we go to Saint Etienne, no pas parler français. Then we’ve been to Barcelona, on a pas de papiers</BR>\r\nLoi Pasqua, Debré et Sarkozy, on est prisonnier. Cause we went here, for the week-end and it be all right</P>\r\n\r\n<P>Double peine et simple condamné, on s’est retrouvés. Hôtel Ibis pour les réfugiés, quatre étoiles pour se faire expulser.</BR>\r\nEspace Schengen, Aznar, Berlusconi, on s’est fait ligoter. Cause we went here, for the week-end and it be all right\r\n</P><P>Dans l’avion, on est isolé ; un oreiller pour ne pas crier. Gare à tes fesses si tu veux protester, tu te fais tabasser…</BR>\r\nBush, Chirac, Poutine et les nantis on a rien mangé. Cause we went here, for the week-end and it be all right</P>\r\n\r\n<P>A la maison, on est pas mieux lotis, ils nous ont tout piqué. Nos richesses, c’est notre survie, ils nous ont tout exporté</BR>\r\nO.M.C, B.M et F.M.I nous ont bien entubés. Cause we went here, for the week-end and it be all right (bis)</P>",
        "<P>En passant devant chez moi, je la croise un peu dérouté...\r\nJe lui propose un café serré, elle me fixe le regard quoi!</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je la recroise au supermarché, elle me regarde interloqué\r\nJe lui propose une poule au blanc, mais elle préfère les restaurants</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je la retrouve un peu plus tard, je lui raconte mes histoires de routard\r\nJe suis un artiste, un écorché, elle travaille chez un banquier</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je lui propose mon lit douillet, elle me repousse effarouchée\r\nJe lui dis que je l\'aime, que je l\'ai dans la peau, mais elle regarde le caniveau</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>",
        "<P>\r\nTant de femmes fatales, se rêvent en hommes politiques\r\nune femme à abattre s\'éprend d\'un homme objet</BR>\r\nles femmes grenouilles préfèrent les hommes à leurs pieds\r\nun homme dans chaque port, de petite vertu</P>\r\n<P>\r\nMais quand une femme pressée, s\'entiche d\'un homme à poigne\r\nune femme de goût se rêve en homme de mauvaise vie</BR>\r\ntant de femmes de confiance, qu\'on a bien trop libéré\r\ndes femmes sandwich, qu\'on ne sait plus trop aimer</BR>\r\n</P>",
        "<P>J’erre dans les rayonnages des supermarchés. Où des poulets, nous attendent bien ordonnés. Numérotés et formatés, nous ne sommes pas différents\r\nSur notre couche de cellophane, la vie glisse lentement. Assis devant notre caddy plein, nous attendons patiemment la fin.</P>\r\n\r\nEt tu cours, après tout ces vies que tu n’auras jamais</BR>\r\nC’est une chanson, sur tous ces échecs, pour tous les gradés, les mauvais\r\n\r\n<P>En rentrant chez moi, j’allume ma télévision: les images qui défilent, y sont bien plus rassurantes. Elles fantasment une vie que nous n’auront jamais: séduisantes et faciles, reflets d’un monde acculé; Derrière ce mur de mensonge, dernier rempart, avant de s’effondrer</P>\r\n\r\nEt tu cours, après toutes ces vies que tu n’auras jamais</BR>\r\nC’est une chanson, sur tous ces échecs, pour tous les gradés, les mauvais"
    ]
</script>
<select name="txtid" onChange="parole(this.value, paroles);" style="border-color: #000000; background-color: #ffffff;">
    <option value="">read the lyrics...</option>
    <option value="">________________</option>
    <option value="">ivresse de la paresse ></option>
    <option value="">---------</option>
    <option value="0">Quittons la France</option>
    <option value="1">loin de toi</option>
    <option value="2">songe des nuits</option>
    <option value="3">bus de nuit</option>
    <option value="4">gaston</option>
    <option value="5">la France a peur</option>
    <option value="6">les sans (papiers)</option>
    <option value="7">une vie</option>
    <option value="8">femmes</option>
    <option value="9">les supermarchés</option>
</select>
<div id="divParole">
</div>
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche4" class="presentation" markdown="1">
### Cd: ivresse de la paresse
#### tatapoum
*Après avoir fait fi de toute pesanteur, le voyage libérateur de celui qui refusait le monde selon Chirafko le plonge dans l'isolement et la mélancolie d'une profonde introspection.*
#### Longueur d'Ondes
[...] *Ainsi Malaussene vagabonde avec humour sur des évènements sérieux. Ses chansons douces amères à rapprocher d’un Jérôme Minière s’imprègnent de l’actu et de ses victimes.*
[voir l'article complet](theme/chroniques/dada_003_ondes.html){. target="_blank"}
#### A découvrir absolument
*Split album entre Malaussene et Banga, l'ivresse de la paresse est la rencontre entre le minimaliste complexe de Jérôme Minére (quittons la France / songe des nuits) et l'action citoyenne.*
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche7" class="presentation" markdown="1">
### They belong to the past...
- Mains d'Oeuvres - Paris (21h - fête de la musique, ) 
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche5" class="presentation" markdown="1">
It was in the heart of his native Normandy, still in his teens, that the
young **Michel Sardon** was moved by the existence of a hitherto unknown music.
It was while reading an issue of Best covering the umpteenth comeback of
Pink Floyd that the young Sardon discovered the existence of the Pixies.
The local media library soon became the scene of encounters with vinyl records.
It was only a few years later that Michel decided to defy the family consensus
that music was a domain reserved for others. And so he launched into his
first chords. The strumming of strings quickly became creative, exorcising
the unfathomable evil that had gnawed at him since his earliest childhood.
Accompanied by a handful of friends, his compositions followed on from his stays
in Normandy and England, ending up in Paris, where Michel launched into M.A.O and D.I.Y!

In 2001, Michel launched **Dadaprod**, seeking to create a new non-commercial
space where music could be exchanged over the Internet or via CD-Rs.
In total opposition to the behemoths of the record industry, dadaprod
castigates the gravediggers of creation, calling for free music where artists
would no longer be bank accounts belonging to the jet-set or elected by TV shows.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche6" class="presentation" markdown="1">
* [download live technical sheet](theme/live/fiche_technique_fr.pdf){. target="_blank"}
* [download stage map](theme/live/plan_scene_fr.pdf){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche10" class="presentation" markdown="1">
### Malaussene, music, pop, DIY, Creative Commons, free
</div>
</div>
