Title: Malaussene
Artist: Malaussene
Date: 2003-03-08 00:00
Category: artiste
Keywords: Malaussene, musique, pop, DIY, Creative Commons, libre
Slug: Malaussene
Lang: fr
Summary: post-pop aux accents subtiles et variés....


<div class="btn-group">
<button type="button" class="btn btn-groupes btn-sm" onclick="change(1);">présentation</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(2);">audio Cd</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(8);">singles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(9);">Video clip</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(3);">paroles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(4);">presse</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(7);">agenda</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(5);">bio</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(6);">fiches techniques</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(10);">tags</button>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche1" class="presentation" markdown="1">
La *faim du travail?* de Brazil, ne nous avait laissé qu'entrapercevoir
le goût de **Sardon** pour le format pop. Avec le projet Malaussene, il laisse
désormais libre cours à ce penchant; Quittant temporairement le post-rock,
pour une post-pop aux accents subtiles et variés.

Tout comme dans brazil, la langue française reste de rigueur. Mais sardon
n'a pas pour autant abandonné sa poésie sociale et il aborde désormais des
sujets graves sur un ton plus léger. La plupart de ses textes ayant été écrits
pendant son tour du monde, ils offrent la place au recul et à l'introspection.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche2" class="presentation" markdown="1">
Retrouvez ci-dessous les participations de Malaussene aux disques de Dadaprod,
que vous trouverez également via la [section dédiée]({category}album) sur
le site ou directement via les liens suivants :
[ivresse de la paresse]({filename}/articles/album/ivresse-de-la-paresse_fr.md).

{!content/include/album/ivresse-de-la-paresse_fr.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche8" class="presentation" markdown="1">
{!content/include/single/dobranoc_fr.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche9" class="presentation" markdown="1">
</div>
</div>

<div class="pres_ntation">
<div id="couche3" class="presentation">
<script type="text/javascript">
    let paroles =  [
        "<P>Après toutes ces révolutions manquées; Vendues à des marchands peu scrupuleux. Ils préfèrent rester chez eux et regarder Laurent Boyer</BR>\r\nDes journaux télévisés au téléthon, ils s’apitoient devant leur télévision. Mais quand un SDF leur fait la manche, ils s’enfuient dans l’indifférence</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>De Libération à Sarkozy, les français ont voté ce qu’on leur avait dit. Et quand vient le temps du mea-culpa, ce sont tous les rois du télé achat</BR>\r\n\r\nLe premier mai, ils défilent : ils ont honte d’être français. Le lendemain, ils se défilent en parlant ciné devant un café</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>Et quand vient le temps des lois sécuritaires, on sabre le champagne sur France Inter. Qu’il soit de gauche ou de droite, ce sont toujours les mêmes costard-cravates</BR>\r\n\r\nQui s’octroient le droit de décider : de la vie et de la mort de milliers d’innocents. Et ce n’est qu’avec guère plus de sentiments, qu’ils sacrifient le bien public aux intérêts privés</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi\r\n\r\n<P>Et quand ils dénoncent la consommation, ce n’est qu’une autre conversation. Ce sont des nostalgiques du grand soir, qui regardent leur nombril devant un miroir</BR>\r\nPuis quand arrive Noël l’opportuniste. Ils s’affairent pour leur grande fête consumériste. Ils s’enorgueillissent d’offrir à leurs descendants, le fruit du travail d’autres enfants</P>\r\n\r\nAlors t’es parti loin, \r\nLa France ça n’était plus pour toi",
        "<P>Je suis loin de toi, dans ces villes\r\nOù je t’aperçois, au fond de ruelles fertiles</BR>\r\nPlus je m’éloigne de toi, et plus je pense à toi. Je me taris de ton absence</P>\r\n\r\n<P>La foule, autour de moi, murmure ton nom, dans des langues que je comprends pas. Quand je pense à ces fesses, que personne ne caresse. J’imagine ta bouche, parcourant le mien\r\n</BR>\r\nCe ne sera pas la première et ce ne sera pas la dernière fois, que je m’éloigne de toi</P>\r\n\r\nMais je voudrais encore une fois te serrer dans mes bras\r\nOh quand je pense à ces fesses que personne ne caresse\r\n\r\n<P>Plus je m’éloigne de toi, et plus je pense à toi\r\nJe me taris de ton absence</P>",
        "<P>Oh songe des nuits, qui sont pour moi des rêves éveillés. Et qui tout comme la pluie, ils s’arrêtent dès qu’ils sont éclairés</BR>\r\nHier je t’ai reconnue. Telle une apparition, tu as disparu. Depuis tu hantes mes nuits, je ne veux que te revoir. Car je sais qu’à minuit, tu reviendras dans le noir</P>\r\n\r\nReste encore auprès de moi, l’aurore se tord elle ne viendra pas\r\n\r\n<P>Oh ma douce de nuit d’amour. Je t’aimerai pour toujours. Je rejoindrai ton royaume secret et on s’aimera pour l’éternité</BR>\r\nMais l’aurore trouble ma vue. Où es-tu ? Je ne te vois plus</P>\r\n\r\nReste encore auprès de moi, l’aurore se tord elle ne viendra pas",
        "<P>J’erre dans les villes, les murs gris me renvoient leur indifférence. Je m’abîme dans ce puits sans fond, m’enfonçant aux hasards des ruelles sombres</P>\r\n\r\nLes bruits de la ville se muent dans un silence abyssal\r\nTout me semble froid et figé, mais il ne faut plus, y penser\r\n\r\n<P>Mais quand vient le soir, je monte dans un bus, le mouvement m’enivre. Recréant la vie, le soleil rougi me réchauffe le cœur</P>\r\n\r\nLes bruits de la ville se muent dans un silence abyssal\r\nTout me semble froid et figé, mais il ne faut plus, y penser",
        "<P>C’est l’histoire d’un con, qu’a jamais dit non. Il travaille du matin au soir, où il broie du noir. Oh un petit noir, au petit matin, histoire de faire passer, cette sale journée</P>\r\n\r\n<P>Cette histoire de con, c’est l’histoire de Gaston; Qu’il soit travailleur, ou commercial manager. Il travaille pour se payer, une machine à laver, la dernière Peugeot, oui c’est un Corniaud !</P>\r\n\r\n<P>Un jour Gaston s’arrête, d’être un social traite. Il ne veut plus travailler, ni se faire exploiter. Il pourra plus se payer de lecteur Dvd; Ses voisins de palier, le traite de PD</P>\r\n\r\n<P>Ces homo Videndis ne sont pas des artistes, qui bossent à la télé, ou dans les cinés. Ce sont des gastons, comme vous et moi, alors ne vous laissez surtout pas: </BR>\r\nêtre pris pour un con…</P>",
        "<P>La France a peur aujourd’hui\r\nTout ça c’est Sarko qui l’a dit. Que t’habites en province, que t’habites à Paris. T’oses plus sortir après minuit</P>\r\n\r\nLa France a peur aujourd’hui\r\n\r\n<P>Le 21 avril, les médias sont surpris. Les Français ont voté nazi. Mais comme le mea-culpa, ils connaissent pas. C’est pas de la faute à ces cons-là</P>\r\n\r\nLa France a le F.N aujourd’hui\r\n\r\n<P>Désormais nous sommes tous des terroristes; Surtout les sans-papiers, les activistes. Sur internet, à la télé, on vous épie, même dans la rue, vous êtes suivis</P>\r\n\r\nSouriez, vous êtes filmés\r\n\r\n<P>J’ai pas choisi de mourir ici\r\nFrance pays des droits de l’homme, pays de Sodome, j’ai pas choisi (bis)\r\n</P>",
        "<P>First we go to Saint Etienne, no pas parler français. Then we’ve been to Barcelona, on a pas de papiers</BR>\r\nLoi Pasqua, Debré et Sarkozy, on est prisonnier. Cause we went here, for the week-end and it be all right</P>\r\n\r\n<P>Double peine et simple condamné, on s’est retrouvés. Hôtel Ibis pour les réfugiés, quatre étoiles pour se faire expulser.</BR>\r\nEspace Schengen, Aznar, Berlusconi, on s’est fait ligoter. Cause we went here, for the week-end and it be all right\r\n</P><P>Dans l’avion, on est isolé ; un oreiller pour ne pas crier. Gare à tes fesses si tu veux protester, tu te fais tabasser…</BR>\r\nBush, Chirac, Poutine et les nantis on a rien mangé. Cause we went here, for the week-end and it be all right</P>\r\n\r\n<P>A la maison, on est pas mieux lotis, ils nous ont tout piqué. Nos richesses, c’est notre survie, ils nous ont tout exporté</BR>\r\nO.M.C, B.M et F.M.I nous ont bien entubés. Cause we went here, for the week-end and it be all right (bis)</P>",
        "<P>En passant devant chez moi, je la croise un peu dérouté...\r\nJe lui propose un café serré, elle me fixe le regard quoi!</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je la recroise au supermarché, elle me regarde interloqué\r\nJe lui propose une poule au blanc, mais elle préfère les restaurants</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je la retrouve un peu plus tard, je lui raconte mes histoires de routard\r\nJe suis un artiste, un écorché, elle travaille chez un banquier</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>\r\n\r\n<P>Je lui propose mon lit douillet, elle me repousse effarouchée\r\nJe lui dis que je l\'aime, que je l\'ai dans la peau, mais elle regarde le caniveau</P>\r\n\r\n<P><I>Pour la, pour la vie, s\'aimer toute une vie</I></P>",
        "<P>\r\nTant de femmes fatales, se rêvent en hommes politiques\r\nune femme à abattre s\'éprend d\'un homme objet</BR>\r\nles femmes grenouilles préfèrent les hommes à leurs pieds\r\nun homme dans chaque port, de petite vertu</P>\r\n<P>\r\nMais quand une femme pressée, s\'entiche d\'un homme à poigne\r\nune femme de goût se rêve en homme de mauvaise vie</BR>\r\ntant de femmes de confiance, qu\'on a bien trop libéré\r\ndes femmes sandwich, qu\'on ne sait plus trop aimer</BR>\r\n</P>",
        "<P>J’erre dans les rayonnages des supermarchés. Où des poulets, nous attendent bien ordonnés. Numérotés et formatés, nous ne sommes pas différents\r\nSur notre couche de cellophane, la vie glisse lentement. Assis devant notre caddy plein, nous attendons patiemment la fin.</P>\r\n\r\nEt tu cours, après tout ces vies que tu n’auras jamais</BR>\r\nC’est une chanson, sur tous ces échecs, pour tous les gradés, les mauvais\r\n\r\n<P>En rentrant chez moi, j’allume ma télévision: les images qui défilent, y sont bien plus rassurantes. Elles fantasment une vie que nous n’auront jamais: séduisantes et faciles, reflets d’un monde acculé; Derrière ce mur de mensonge, dernier rempart, avant de s’effondrer</P>\r\n\r\nEt tu cours, après toutes ces vies que tu n’auras jamais</BR>\r\nC’est une chanson, sur tous ces échecs, pour tous les gradés, les mauvais"
    ]
</script>
<select name="txtid" onChange="parole(this.value, paroles);" style="border-color: #000000; background-color: #ffffff;">
    <option value="">lire les textes...</option>
    <option value="">________________</option>
    <option value="">ivresse de la paresse ></option>
    <option value="">---------</option>
    <option value="0">Quittons la France</option>
    <option value="1">loin de toi</option>
    <option value="2">songe des nuits</option>
    <option value="3">bus de nuit</option>
    <option value="4">gaston</option>
    <option value="5">la France a peur</option>
    <option value="6">les sans (papiers)</option>
    <option value="7">une vie</option>
    <option value="8">femmes</option>
    <option value="9">les supermarchés</option>
</select>
<div id="divParole">
</div>
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche4" class="presentation" markdown="1">
### Cd: ivresse de la paresse
#### tatapoum
*Après avoir fait fi de toute pesanteur, le voyage libérateur de celui qui refusait le monde selon Chirafko le plonge dans l'isolement et la mélancolie d'une profonde introspection.*
#### Longueur d'Ondes
[...] *Ainsi Malaussene vagabonde avec humour sur des évènements sérieux. Ses chansons douces amères à rapprocher d’un Jérôme Minière s’imprègnent de l’actu et de ses victimes.*
[voir l'article complet](theme/chroniques/dada_003_ondes.html){. target="_blank"}
#### A découvrir absolument
*Split album entre Malaussene et Banga, l'ivresse de la paresse est la rencontre entre le minimaliste complexe de Jérôme Minére (quittons la France / songe des nuits) et l'action citoyenne.*
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche7" class="presentation" markdown="1">
### Ils appartiennent déjà au passé...
- Mains d'Oeuvres - Paris (21h - fête de la musique, ) 
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche5" class="presentation" markdown="1">
C’est au cœur de sa Normandie natale, encore en pleine adolescence, que le
jeune **Michel Sardon** s’émeut de l’existence d’une musique jusqu’alors inconnue.
C'est à la lecture d’un numéro de Best couvrant un énième come-back du groupe
Pink Floyd que le jeune sardon découvre l'existence des Pixies.
La médiathèque municipale de la ville voisine, devient alors bien vite le
théâtre de rencontres avec des vinyles, puis des Cds estampillés "musique pas comme les autres",
comme autant de remparts face à la coercition du milieu éducatif et familiale.
Et ce n’est que quelques années plus tard que Michel, décide de braver le
consensus familiale selon lequel la musique serait un domaine réservé aux autres.
Il se lance alors dans ses premiers accords. Le frôlement des cordes devient
rapidement créatif, exorcisant ainsi ce mal insondable qui le ronge depuis sa
plus tendre enfance. Accompagné par quelques amis, les compositions se succèdent
au gré de ses séjours en Normandie et en Angleterre, pour finir à Paris où Michel
se lance dans la M.A.O et le D.I.Y !

Sur sa lancée, Michel lance en 2001 **Dadaprod**, cherchant à créer un nouvel
espace non marchand où la musique s’échange sur Internet ou via des Cd-R.
En totale opposition par rapport aux mastodontes de l’industrie du disque,
dadaprod fustige les fossoyeurs de la création, revendiquant une musique
libre où les artistes ne seraient plus des comptes en banque appartenant
à la jet-set où des élus dans des programmes autant racoleurs que mercantiles.
Michel décide alors de décliner sa sensibilité musicale au gré de plusieurs projets solos,
comme banga (créant ainsi le mouvement punk-folk) et brazil, plus ouvert à la musique instrumentale.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche6" class="presentation" markdown="1">
* [Téléchargez la fiche technique](theme/live/fiche_technique_fr.pdf){. target="_blank"}
* [Téléchargez le plan de scène](theme/live/plan_scene_fr.pdf){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche10" class="presentation" markdown="1">
### Malaussene, musique, pop, DIY, Creative Commons, libre
</div>
</div>
