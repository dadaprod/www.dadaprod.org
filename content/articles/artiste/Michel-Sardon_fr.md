Title: Michel Sardon
Artist: Michel Sardon
Date: 2007-09-14 00:00
Category: artiste
Keywords: Michel Sardon, musique, DIY, Creative Commons, libre, écologie, anarchie, résistance, luttes, Dadvsi, DRM, politique, promesse, universal, 4x4, clip, musique, Paris, Auvergne, pétrole, planete, réchauffement, climat
Slug: Michel-Sardon
Lang: fr
Summary: Chanteur enragé, perdu en Auvergne...


<div class="btn-group">
<button type="button" class="btn btn-groupes btn-sm" onclick="change(1);">présentation</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(2);">audio Cd</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(8);">singles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(9);">Video clip</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(3);">paroles</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(4);">presse</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(7);">agenda</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(5);">bio</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(6);">fiches techniques</button>
<button type="button" class="btn btn-groupes btn-sm" onclick="change(10);">tags</button>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche1" class="presentation" markdown="1">
Parti sur une pop boisée aux accents de Low Barlow ou des Modern Lovers,
la musique de Michel Sardon s'est peu à peu étoffée, affirmant d'une part un son plus rock et s'ouvrant également à de nouveaux rythmes et à de nouvelles ambiances...

Après avoir voyagé le temps d'un disque dans la peau de **Malaussene**,
Michel Sardon s'est intéressé à la chanson française et à des influences ragga rock.
La musique de **Michel sardon** ressemble donc de plus en plus à un melting pop,
exprimant parfois la mélancolie mais souvent la rage de vivre dans une société
pour laquelle le jeune Sardon ne semble pas être fait.

Le choix de plus en fréquent de la langue française permet ainsi de décliner
une poésie sociale, en rupture avec le romantisme mâtinant la scène française (nouvelle ou ancienne).
**Michel Sardon** tente ainsi de faire le grand écart entre les bérus, Brassens et les Pixies...
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche2" class="presentation" markdown="1">
Retrouvez ci-dessous les participations de Michel Sardon aux disques de Dadaprod,
que vous trouverez également via la [section dédiée]({category}album) sur
le site ou directement via les liens suivants :
[les champs de la résistance (liberterre)]({filename}/articles/album/les-champs-de-la-resistance_fr.md),
[descendons dans la rue]({filename}/articles/album/descendons-dans-la-rue_fr.md).

{!content/include/album/les-champs-de-la-resistance_fr.md!}
{!content/include/album/descendons-dans-la-rue_fr.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche8" class="presentation" markdown="1">
{!content/include/single/balance-ton-point_fr.md!}

{!content/include/single/jeudi-noir_fr.md!}

{!content/include/single/stop-drm_fr.md!}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche9" class="presentation" markdown="1">
{!content/include/video/le-triple-A_fr.md!}
{!content/include/video/fuck-the-4x4_fr.md!}
</div>
</div>

<div class="pres_ntation">
<div id="couche3" class="presentation">
<script type="text/javascript">
    let paroles =  [
        "<p>On les retrouve au mois de mai, tous les 5 ans à la télé<br>à faire des promesses stériles, qui savent qui ne tiendront jamais<br>Les politiques font des promesses, quand tant d\'autres vivent la détresse,<br>liée à des politiques iniques qui favorisent ceux qu\'ont du fric</p><p>Ils nous annoncent dans les journaux, la fin du chômage, pour bientôt<br>Mais ils ne comprendront jamais, qu\'on ne veut plus travailler<br>Pour des tyrans, pour trois kopecks, horaires flexibles et prises de bec<br>sous la pression d\'un manager, qui licencie pour se faire du beurre</p><p><em>Descendons tous dans la rue, qu\'attendons-nous pour mettre le feu<br>Votez à gauche, c\'est bien peu, manifestez c\'est déjà mieux</em></p><p>Sarko, Chirac ou Hollande sont imposés à coup de sondage<br>Récompensant les collabos, de tout temps et de tout âge<br>Les politiques sont des pantins, obéissant à des larbins<br>rythmant leur pas sur des données, gouvernées par des marchés</p><p><em>Descendons tous dans la rue, qu\'attendons-nous pour mettre le feu<br>Votez à gauche, c\'est bien peu, manifestez c\'est déjà mieux</em></p><p>Ils nous invitent à voter pour l\'ordre et la sécurité<br>à l\'aide de médias policés, en faveur d\'états policiers<br>Les policiers sont partout: dans les séries, chez Marc Dutroux<br>Quand on ne voit pas des SS, ce sont des cars de CRS</p><p><em>Descendons tous dans la rue, qu\'attendons-nous pour mettre le feu<br>Votez à gauche, c\'est bien peu, manifestez c\'est déjà mieux</em></p>",
        "<p>La dette est un plat, qui se prépare à l’avance<br/>\r\npour avoir de la saveur, et éviter les dépenses<br/>\r\npour plaire aux plus riches, et ruiner la France<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nEn France cela fait, environ trente années<br/>\r\nque la recette, a été pensée<br/>\r\nC\'est la concurrence, libre, et non faussée<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nAvant de mettre au chaud, il faut la liberté<br/>\r\nde circulation, des capitaux<br/>\r\nC\'est la concurrence, libre, et non faussée<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nLes ingrédients prêts, il faut baisser<br/>\r\nles impôts des riches, et interdire<br/>\r\nla création monétaire, par un État<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nDans un même temps, il faut augmenter<br/>\r\nles plus hauts salaires, autour de 30%<br/>\r\nTout en limitant à 3%<br/>\r\nla hausse des 90% restant<br/>\r\n<br/>\r\nAinsi la part des salaires, dans la sauce, « PIB »<br/>\r\ndiminue au profit des dividendes, aux actionnaires<br/>\r\nL’assaisonnement, est presque parfait<br/>\r\nentre 8 et 9%, de transfert<br/>\r\n<br/>\r\nMais les plus riches peuvent encore, s’enrichir<br/>\r\nEn prêtant à l’État, en toute, sécurité<br/>\r\nC\'est la garantie, d\'un bon taux, d’intérêt<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nSi la cuisson s’est bien déroulée<br/>\r\nla part des recettes a enfin chuté<br/>\r\nLa part des dépenses, elle, s\'est stabilisée<br/>\r\nSortie du four, le déficit, a gonflé<br/>\r\n<br/>\r\nC’est le signe, votre dette est prête ! <br/>\r\nPour mettre en appétit, les conseils du chef<br/>\r\nun zeste de culpabilisation, pour nos enfants<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nAssaisonner, d’une cuillerée<br/>\r\nde suppression des droits, et de nos acquis<br/>\r\nNous vivons au, dessus de nos moyens<br/>\r\nAh Ah, le Triple A<br/>\r\n<br/>\r\nPour une meilleure, digestion<br/>\r\npour les peuples, un verre de division<br/>\r\nOn ne va pas payer, pour ces fainéants !<br/>\r\nAh Ah, le Triple A</p>",
        "<p>Je mange grâce au nucléaire, je m\'éclaire au nucléaire, je me chauffe au nucléaire<br/>\r\nJe vis du nucléaire mais je meure au Niger</p>\r\n\r\n<p>Internet au nucléaire, lampadaires au nucléaire, autoroutes au nucléaire<br/>\r\nLes lobbies du nucléaire, dépouillent même le Niger</p>\r\n\r\n<p>Des bombes au nucléaire, des centrales nucléaires, sous-marins nucléaires<br/>\r\nDes déchets qu\'on enterre, dont on ne sait plus que faire</p>\r\n\r\n<p>Écran plasma au nucléaire, ou éclairé au nucléaire, qui font de la pub pour le nucléaire,<br/>\r\nLes lobbies du nucléaire, éclaire même le désert</p>\r\n\r\n<p>Les fuites du nucléaire, incident nucléaire, catastrophe nucléaire,<br/>\r\nLes nuages nucléaires, Y\'a qu\'en France, qu\'on les enterre</p>\r\n\r\n<p>Autriche sans nucléaire, Portugal sans nucléaire, Irlande sans nucléaire<br/>\r\nItalie sans nucléaire et pour la France :EPR !</p>\r\n\r\n<p>Dans les centrales nucléaires, les emplois sont précaires, sécurité lacunaire<br/>\r\ngouvernance suicidaire, des lobbies nucléaires</p>\r\n\r\n<p>Les essais nucléaires, irradiation nucléaire, leucémie au nucléaire,<br/>\r\nFukushima, Tchernobyl, sont les cancers du nucléaire</p>\r\n\r\n<p><em>plus de bruit, contre le nucléaire</em></p>",
        "<p>Je ne peux plus vivre avec toutes ces voitures, ça pollue ma ville et la nature</p>\r\n<p>Car free family, Car free family, People United, People United</p>\r\n<p>Roulons à vélo, Roulons à vélo; Même s\'il ne fait pas beau, faison du vélo</p>\r\n<p>When people in this land, when people united<br/>\r\nWe will all then, change this world</p>\r\n<p>Tu l\'as prend par la main, comme une vipère au poing<br/>\r\nTu écris sur les murs en crachant sur ces ordures<br/>\r\net il court, elle accourt dans les rues sans détour<br/>\r\nles politiques abusent et les médias s\'en amusent<br/>\r\nUn citoyen s\'éveille, un consommateur sort du sommeil<br/>\r\nc\'est le droit de propriété, qu\'il nous faut supprimer<br/>\r\nIls s\'attrapent dans les rues, l\'insolence bien en vue<br/>\r\nIls commencent à parler, à rire, à échanger<br/>\r\nL\'an 01 est né dans une Europe mondialisée<br/>\r\nLes pavés sont retournés sans la sécurité<br/>\r\ndes potagers s\'improvisent, on abandonne les devises<br/>\r\nIl était dactylo, elle était ménaco<br/>\r\nIls entrent dans les usines et arrêtent les machines<br/>\r\nLes bourgeois sont ruinés, leurs possessions partagées<br/>\r\nLa propriété abolie, il n\'y a plus de sans abri<br/>\r\nEt ils sèment, ils essaiment dans les rues, dans les tours<br/>\r\nUne génération est née dans un bordel organisé</p>",
        "<p>Vous savez pas, c’qu’ils ont décidé</BR>\r\nCe qu’ils sont en train de nous faire avaler</BR>\r\nUne couleuvre de la taille d’un champ de blé</BR>\r\nDont les graines n’appartiennent plus aux fermiers</p>Les nouveaux aliments ingérés</BR>\r\nNe sont pas bons pour notre santé</BR>\r\nMais pour le porte-feuille des multinationales</BR>\r\nC’est l’équivalent de c’que tu crois être de la balle</BR>\r\n<P>\r\nMais moi j’aime pas</BR>\r\nLes Ogm parce</BR>\r\nQue c’est pas bon pour mon système immunitaire<br>\r\nMais moi j’aime pas</BR>\r\nLes Ogm parce</BR>\r\nQue c’est pas bon pour ma mère la terre</BR>\r\n</P><P>\r\nLes gènes des poissons, résistent au froid</BR>\r\nMais les pauvres fraises, qui n’en ont pas</BR> \r\nFaudrait les aider à pousser, en Alaska</BR>\r\nParce que l’importation est trop élevée là-bas </BR>\r\nLes grenouilles, résistent aux insectes</BR>\r\nMais pas les plantes ça vous inquiète</BR>\r\nAlors pour pas qu’elle se fasse, trop attaquer</BR>\r\nOn a qu’à les engrenouiller par gènes interposés</BR>\r\n</P><P>\r\nMais moi j’aime pas</BR>\r\nLes Ogm parce</BR>\r\nQue c’est pas bon pour mon système immunitaire<br>\r\nMais moi j’aime pas</BR>\r\nLes Ogm parce</BR>\r\nQue c’est pas bon pour ma mère la terre</BR>\r\n</P><P>\r\nOn nous dit, qu’on est des froussards</BR>\r\nQue l’progrès nous fait peur, comme la nuit noire</BR>\r\nEt en effet les OGM ça fait flipper</BR>\r\nMon corps qui est déjà gra\'vement pollué</BR>\r\nC’est pas constructif, d’être contre</BR>\r\nAlors disons que je suis, pour la vie</BR>\r\nEt dans cette course contre, la montre</BR>\r\nLes OGM je les refuse, je les renie.</p>",
        "<p>Je suis un être à la recherche, d\'un appartement à louer\r\n<br/>\r\nPas simplement d\'une masure, je veux sortir de la précarité\r\n</p>\r\n<p>j\'en ai assez de ce squat, et de vivre toutes ces galères<br/>\r\nils me disent de rester dans la norme, et de payer pour une misère</p>\r\n\r\n<p>Alors je cherche et je trouverai cette apart qui me tente tant\r\n<br/>\r\nAlors je cherche et je trouverai cette apart qui me tente tant\r\n<br/>\r\nQui me tente tant han han</p>\r\n\r\n<p>Particulier à particulière cherche particulière à particulier\r\n<br/>\r\nUne fiche de paie, un garant blindé et une bonne dose de savoir faire\r\n, Savoir faire\r\n</p>\r\n\r\n<p>Vous comprendrez que de tels projets, parfois sont difficiles à réaliser<br/>\r\nIls sont au\'tour de moi si fragiles, ce n\'est pas parmi eux que je trouverai</p>\r\n\r\n<p>Je dois trouver de nouveaux horizons\r\n\r\nMais je finis parfois par tourner en rond</p>\r\n\r\n<p>Alors je cherche et je trouverai cette apart qui me tente tant\r\n<br/>\r\nAlors je cherche et je trouverai cette apart qui me tente tant\r\n<br/>\r\nQui me tente tant han han</p>\r\n\r\n<p>Particulier à particulière cherche particulière à particulier\r\n<br/>\r\nUne fiche de paie, un garant et une bonne dose de savoir faire\r\n, Savoir faire</p>",
        "<p>Tu es né à Beyrouth, dans la peur et dans le doute<br />\r\nTrois jours après, vie terminée, dans un hôpital bombardé<br />\r\nLes politiques ensanglantés, se sont à peine, excusés<br />\r\nUne fin fatale, plutôt brutale, un dégât, collatéral<br />\r\nQuand Israël assassine, à Gaza ou à Jénine<br />\r\nÀ l\'ONU, c\'est résolue, Sion n\'est pourtant pas, au dessus</p>\r\n<p>Pas d\'estime, pour la Palestine,<br />quand on négocie, on oublie</p>\r\n<p>Résistance vaincra, à Paris ou à Gaza<br />\r\nRésistance met en déroute, même à Beyrouth</p>\r\n<p>Tu est mort à Beyrouth, sans un mot et sans un doute<br />\r\nTrois jours de vie, c\'est trop court, <br />les bombes épargnent les, long discours<br />\r\nQuand des hommes résistent, on les nomme terroriste<br />\r\nEt quand un état assassine, on l\'invite avec Poutine<br />\r\nLes médias oublient vite, leurs héros activistes<br />\r\nDans les maquis, la dynamite,<br /> n\'était pourtant pas en terre cuite</p>\r\n\r\n<p>Résistance vaincra, à Paris ou à Gaza<br />\r\nRésistance met en déroute, même à Beyrouth</p>\r\n\r\n<p>Dans les amphis, résistance, à Gaza, résistance<br />\r\nEn Tchéchénie, résistance, Oxaca, résistance<br />\r\nDans les banlieux, résistance, même à Paris, résistance<br />\r\nAvec Bourdieu, résistance, en Algérie, résistance</p>\r\n\r\n<p>Résistance vaincra, à Paris ou à Gaza<br />\r\nRésistance met en déroute, même à Beyrouth</p>\r\n\r\n<p>Tu est mort à Beyrouth, sans un mot, dans un souffle<br />\r\nTrois jours ce n\'est pas assez, afin d\'être assassiné<br />\r\nQuand un enfant meurt à Jénine, c\'est l\'humanité, qu\'on assassine<br />\r\nIci ou ailleurs, même au Liban, (la) démocratie tue, bien souvent<br />\r\nFace aux sionistes, tu résistes, quand les bombes persistent<br />\r\nEn Argentine, tu insistes, A bas le capitalisme</p>\r\n\r\n<p>Résistance vaincra, à Paris ou à Gaza<br />\r\nRésistance met en déroute, même à Beyrouth</p>\r\n\r\n<p>Tu est mort à Beyrouth, dans un hôpital en déroute<br />\r\nTrois jours de vie, c\'est trop court, les bombes épargnent les, long discours<br />\r\nQuand un soldat entre à Gaza, l\'humanité perd un bras<br />\r\nHiroshima mon amour, little boy pour toujours<br />\r\nIsraël, État criminel, États-Unis, barbarie<br />\r\nnon à leur démocratie, non à leur technocratie</p>\r\n\r\n<p>Résistance vaincra, à Paris ou à Gaza<br />\r\nRésistance met en déroute, même à Beyrouth</p>",
        "Téléchargez moi, je suis légal\r\nSi vous cliquez 100 fois, vous pourrez gagner 10  balles\r\n\r\nTéléchargez moi, ça m\'est égal\r\nde me retrouver ranger à coté de pierre Bachelet\r\n\r\nTéléchargez moi, ca m\'fera pas de mal\r\nSi vous parlez de moi, j\'aurai peut-être un édito\r\n\r\nAux fous du roi, sur France Inter, une affiche dans le métro et une invit\' chez Drucker\r\n\r\nTéléchargez moi, j\'suis sur le net\r\nJ\'ai des friends sur Myspace, oui j\'suis un type honnête\r\n\r\nTéléchargez moi, Téléchargez moi\r\nTéléchargez .. moi\r\n\r\nTéléchargez moi, je veux qu\'on m\'aime\r\nen format libre de droit, certifié sans DRM\r\n\r\nTéléchargez moi, vous ne me perdrez pas\r\nquand vous changerez de Pc, quand votre disque aura crashé\r\n\r\nTéléchargez moi, je suis compatible\r\nsur les baladomobiles et même sur les distib Linux\r\n\r\nSur la Debian, sur la slackware, j\'suis archi compatible, oui je suis un truc terrible\r\n\r\nTéléchargez moi, je suis gratuit\r\nsans rootkit de Sony, oui je suis un bon partie\r\n\r\nTéléchargez moi, Téléchargez moi\r\n(Téléchargez .. moi)\r\n\r\nTéléchargez moi, découvrez moi\r\navant que la gloire ne m\'affecte, avant qu\'on m\'colle une étiquette\r\n\r\nTéléchargez moi, invitez moi\r\ndans tous les baltringues, avant qu\'j devienne une marque de fringue\r\navant qu\'j\' passe à la télé, dans une publicité pour des produits laitiers\r\n\r\nTéléchargez moi, podcastez moi\r\navant quj\'en fasse mon beurre et que j\'chante aux restos du coeur\r\navant quj\'devienne un truc pourri, un produit de vivendi à la star academy\r\n\r\nTéléchargez moi, emulez moi,\r\navant que je sois engagé, tant que je reste un enragé\r\nun lecteur du plan B, un vendeur d\' CQFD, un enragé, un enragé !!",
        "<p>Déjà tout petit, j\'étais fan de 4x4</br>\r\nmême pour une copie, j\'me coupais les cheveux quatre</br>\r\noh mon joli Toyota, non, non je ne le prêtais pas</p><p>\r\nMaintenant j\'suis grand, je gagne des millions de francs</br>\r\nj\'ai changé d\'échelle, je joue à la taille humaine</br>\r\nnon, je ne regrette rien, maintenant le monde m\'appartient\r\n</p><p>\r\nFuck, the 4x4, Fuck the 4x4, Fuck the 4x4, Fuck the 4x4\r\n</p><p>\r\nDu haut de mon char, je domine la circulation</br>\r\net au moindre écart, je donne un bon coup de klaxon</br>\r\nOh mon joli toyota, non, non je ne le prête pas\r\n</p><p>\r\nAvec mon blindé, je voyage en sécurité</br>\r\net pour se garer, ce n\'est vraiment pas compliqué</br>\r\nn\'importe où sur le trottoir, moi je me gare au hasard</br>\r\n</p><p>\r\nFuck, the 4x4, Fuck the 4x4, Fuck the 4x4, Fuck the 4x4</p><p>\r\n\r\nDepuis ma promotion, j\'ai pris la climatisation</br>\r\nmaintenant dans les bouchons, je ne sens plus la pollution</br>\r\net quand vraiment, ca va mal, j\'écoute, un disque Universal</p><p>\r\n\r\nMaintenant qu\'y a plus d\' pétrol</br>\r\nJe n\'ai plus besoin de bagnole</br>\r\nAvec le pic d\'hubert, il va falloir se mettre au vert</br>\r\n\r\nEt mon joli toyota, il ne me servira..  pas</p>"
    ]
</script>
<select name="txtid" onChange="parole(this.value, paroles);" style="border-color: #000000; background-color: #ffffff;">
    <option value="">lire les textes...</option>
    <option value="">________________</option>
    <option value="">Les champs de la résistance (liberterre) ></option>
    <option value="">---------</option>
    <option value="0">descendons dans la rue</option>
    <option value="1">AAA</option>
    <option value="2">A bas le nucléaire</option>
    <option value="3">La décroissance</option>
    <option value="1">Le Triple A</option>
    <option value="4">OGM Pas</option>
    <option value="5">Jeudi Noir</option>
    <option value="6">Résistance</option>
    <option value="">________________</option>
    <option value="">descendons dans la rue ></option>
    <option value="">---------</option>
    <option value="0">descendons dans la rue</option>
    <option value="7">telechargez moi</option>
    <option value="8">fuck the 4x4</option>
</select>
<div id="divParole">
</div>
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche4" class="presentation" markdown="1">
### Cd: descendons dans la rue

#### The French Touch
[...] *C'est ici à la chanson française que s'attaque Michel, et si la droite avait son Sardou, la (vraie) gauche a désormais Sardon.*
[...] *Le message est ici bien évidemment le plus important et Michel Sardon prend un malin plaisir à le faire passer en parodiant un chanteur ou un style sur chaque titre. Du grand art !*

#### Silence
*En quatre titres, à l'acent plutôt rock, mâtiné de reggae, le chanteur dénonce les mensonges de politiques (ah les "beaux" discours de Nicolas et de Ségolène !) et en appelle à la résistance dans la rue, s'en prend à l'industrie du disque ainsi qu'aux fans de 4x4.*
[voir l'article complet](theme/chroniques/dada_005_silence.html){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche7" class="presentation" markdown="1">
### Ils appartiennent déjà au passé...
- Raymond Bar - Clermont-Ferrand (20h00 - concert de soutien aux prisonniers politiques [77 Av E. Michelin], 5 euros)
- Espace chambon - Cusset (18h - Contre sommet sur l'immigration, paf libre)
- Libre Acces - Paris (19h - accès libre - http://www.libreacces.org/, )
- FRAP 2008 - Tv Bocal - Paris (19h - 12 villa Riberolle 75020 [FRAP 2008], PAF libre)
- scène ouverte - Paris (21h30 - 7/9 rue Francis de Pressensé 75014 [scène ouverte], )
- L'Antirouille - Paris (21h00 - 5 rue Moret - 75011 Paris [M° : Oberkampf, Menilmontant], Entrée Libre)
- Le Pix Bar - Paris (20h - 49 rue Pixerecourt 75020, PAF libre)
- au magique - Paris (21h30 - http://www.aumagique.com/, PAF libre) 
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche5" class="presentation" markdown="1">
C’est au cœur de sa Normandie natale, encore en pleine adolescence, que le
jeune **Michel Sardon** s’émeut de l’existence d’une musique jusqu’alors inconnue.
C'est à la lecture d’un numéro de Best couvrant un énième come-back du groupe
Pink Floyd que le jeune sardon découvre l'existence des Pixies.
La médiathèque municipale de la ville voisine, devient alors bien vite le
théâtre de rencontres avec des vinyles, puis des Cds estampillés "musique pas comme les autres",
comme autant de remparts face à la coercition du milieu éducatif et familiale.
Et ce n’est que quelques années plus tard que Michel, décide de braver le
consensus familiale selon lequel la musique serait un domaine réservé aux autres.
Il se lance alors dans ses premiers accords. Le frôlement des cordes devient
rapidement créatif, exorcisant ainsi ce mal insondable qui le ronge depuis sa
plus tendre enfance. Accompagné par quelques amis, les compositions se succèdent
au gré de ses séjours en Normandie et en Angleterre, pour finir à Paris où Michel
se lance dans la M.A.O et le D.I.Y !

Sur sa lancée, Michel lance en 2001 **Dadaprod**, cherchant à créer un nouvel
espace non marchand où la musique s’échange sur Internet ou via des Cd-R.
En totale opposition par rapport aux mastodontes de l’industrie du disque,
dadaprod fustige les fossoyeurs de la création, revendiquant une musique
libre où les artistes ne seraient plus des comptes en banque appartenant
à la jet-set où des élus dans des programmes autant racoleurs que mercantiles.
Michel décide alors de décliner sa sensibilité musicale au gré de plusieurs projets solos,
comme banga (créant ainsi le mouvement punk-folk) et brazil, plus ouvert à la musique instrumentale.
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche6" class="presentation" markdown="1">
* [Téléchargez la fiche technique](theme/live/fiche_technique_fr.pdf){. target="_blank"}
* [Téléchargez le plan de scène](theme/live/plan_scene_fr.pdf){. target="_blank"}
</div>
</div>

<div class="pres_ntation" markdown="1">
<div id="couche10" class="presentation" markdown="1">
### **Michel Sardon**, musique, DIY, Creative Commons, libre, écologie, anarchie, résistance, luttes, Dadvsi, DRM, politique, promesse, universal, 4x4, clip, musique, Paris, Auvergne, pétrole, planete, réchauffement, climat
</div>
</div>
