Title: Premières nouvelles
Artist: Brazil et Banga
Image: dada_001
Price: 5 euros
Tags: mail-order
Date: 2001-01-01 00:00
Category: album
Keywords: Brazil, Banga, compilation, commander, DIY, Creative Commons, libre
Slug: premieres-nouvelles
Lang: fr
Summary: Première compilation "maison" de dadaprod. Elle est composée d'un patchwork des premières compositions des groupes...

{!content/include/album/premieres-nouvelles_fr.md!}
