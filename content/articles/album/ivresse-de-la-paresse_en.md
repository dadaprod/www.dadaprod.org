Title: Ivresse de la paresse
Artist: Banga et Malaussene
Image: dada_003
Price: 5 euros
Tags: mail-order
Date: 2004-01-01 00:00
Category: album
Keywords: Banga, Malaussene, scène française, guitare, tour du monde, DIY, Creative Commons, libre
Slug: ivresse-de-la-paresse
Lang: en
Summary: After six months of travelling with his guitar, sardon come back with lots of new ideas...

{!content/include/album/ivresse-de-la-paresse_en.md!}
