Title: A Bas Travailler (live)
Artist: Brazil
Image: dada_004
Price: 5 euros
Tags: mail-order
Date: 2005-01-01 00:00
Category: album
Keywords: Brazil, travail, post-rock, looping, scène française, commander, DIY, Creative Commons, libre
Slug: A-Bas-Travailler-live
Lang: en
Summary: Record coming from rehearsals for Brazil's gig...

{!content/include/album/A-Bas-Travailler_en.md!}
