Title: La faim du travail
Artist: Brazil
Image: dada_002
Price: 5 euros
Tags: mail-order
Date: 2002-01-01 00:00
Category: album
Keywords: Brazil, post-rock, travail, social, luttes, commander, DIY, Creative Commons, libre
Slug: la-faim-du-travail
Lang: en
Summary: First album of Brazil, which takes you into the heart of a social post-rock on revolt against a society built around work...

{!content/include/album/la-faim-du-travail_en.md!}
