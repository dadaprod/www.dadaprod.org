Title: Les champs de la résistance (liberterre)
Artist: Michel Sardon
Image: dada_006
Price: 8 euros
Tags: mail-order
Date: 2016-01-01 00:00
Category: album
Keywords: résistance, Michel Sardon, Sardon, looping, scène française, compilation, commander, DIY, Creative Commons, libre
Slug: les-champs-de-la-resistance
Lang: en
Summary: You can already pre-order the brand new album of Michel Sardon...

{!content/include/album/les-champs-de-la-resistance_en.md!}
