Title: descendons dans la rue
Artist: Michel Sardon
Image: dada_005
Price: 5 euros
Tags: mail-order
Date: 2007-01-01 00:00
Category: album
Keywords: résistance, Michel Sardon, Sardon, looping, scène française, compilation, commander, DIY, Creative Commons, libre
Slug: descendons-dans-la-rue
Lang: en
Summary: As Banga debut were inspired by Low barlow and the anti-folk scene. The music grow up and is now more rock oriented. Some other influences also showed up, especially inspired by french song and ragga roots. So Banga music is now a sort of melting pot.. The lyrics are also more and more written in french, giving more precision to the social poetry of this young sardon.

{!content/include/album/descendons-dans-la-rue_en.md!}
