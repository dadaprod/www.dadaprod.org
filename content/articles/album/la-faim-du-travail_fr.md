Title: La faim du travail
Artist: Brazil
Image: dada_002
Price: 5 euros
Tags: mail-order
Date: 2002-01-01 00:00
Category: album
Keywords: Brazil, post-rock, travail, social, luttes, commander, DIY, Creative Commons, libre
Slug: la-faim-du-travail
Lang: fr
Summary: Premier album de brazil, qui vous fait pénétrer au cœur d'un post-rock social en révolte contre une société bâtie autour du travail

{!content/include/album/la-faim-du-travail_fr.md!}
