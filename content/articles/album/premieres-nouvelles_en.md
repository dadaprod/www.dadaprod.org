Title: Premières nouvelles
Artist: Brazil et Banga
Image: dada_001
Price: 5 euros
Tags: mail-order
Date: 2001-01-01 00:00
Category: album
Keywords: Brazil, Banga, compilation, commander, DIY, Creative Commons, libre
Slug: premieres-nouvelles
Lang: en
Summary: First compilation of Dadaprod, featuring a patchwork of the bands' early compositions...

{!content/include/album/premieres-nouvelles_en.md!}
