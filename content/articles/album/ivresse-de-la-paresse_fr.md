Title: Ivresse de la paresse
Artist: Banga et Malaussene
Image: dada_003
Price: 5 euros
Tags: mail-order
Date: 2004-01-01 00:00
Category: album
Keywords: Banga, Malaussene, scène française, guitare, tour du monde, DIY, Creative Commons, libre
Slug: ivresse-de-la-paresse
Lang: fr
Summary: Après plus de six mois passés à parcourir le monde avec sa guitare, sardon est revenu avec pleins d’idées. Certains morceaux restant dans l’esprit anti-folk de banga, d’autres s’engageant dans des ères plus sophistiquées et qui seront désormais estampillés Malaussene...

{!content/include/album/ivresse-de-la-paresse_fr.md!}
