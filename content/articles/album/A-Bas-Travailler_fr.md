Title: A Bas Travailler (live)
Artist: Brazil
Image: dada_004
Price: 5 euros
Tags: mail-order
Date: 2005-01-01 00:00
Category: album
Keywords: Brazil, travail, post-rock, looping, scène française, commander, DIY, Creative Commons, libre
Slug: A-Bas-Travailler-live
Lang: fr
Summary: Un peu plus de deux ans après la parution de « la faim du travail? », il nous a paru nécessaire d'éditer un nouveau disque de Brazil. Le son de Brazil s'étant en effet considérablement étoffé depuis ce premier enregistrement, les anciens morceaux ont peu à peu donné naissance à de nouvelles pièces. Cette maturation ayant notamment pris forme lors de répétitions en vue des quelques concerts qui ont jalonnés la vie du projet, il nous a semblé logique d'enregistrer ce disque dans les conditions du live. Préférant ainsi crée à partir des imperfections, plutôt que de chercher à les éviter...

{!content/include/album/A-Bas-Travailler_fr.md!}
