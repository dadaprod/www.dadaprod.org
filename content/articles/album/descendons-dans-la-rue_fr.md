Title: Descendons dans la rue
Artist: Michel Sardon
Image: dada_005
Price: 5 euros
Tags: mail-order
Date: 2007-01-01 00:00
Category: album
Keywords: résistance, Michel Sardon, Sardon, looping, scène française, compilation, commander, DIY, Creative Commons, libre
Slug: descendons-dans-la-rue
Lang: fr
Summary: Après avoir voyagé le temps d'un disque dans la peau de Malaussene, Michel Sardon revient avec son banga looping band (en attendant que d'autres musiciens rejoignent l'aventure pour un développement scénique). Alors qu'un son plus rock s'affirme sur certains morceaux, d'autres influences (chanson française, ragga) se développent également, poursuivant ainsi le travail entamé sur des morceaux tels que les sans ou Gaston sur l' « ivresse de la paresse ». Le choix de plus en fréquent de la langue française permet ainsi de décliner une poésie sociale, en rupture avec le romantisme mâtinant la scène française (nouvelle ou ancienne).

{!content/include/album/descendons-dans-la-rue_fr.md!}
