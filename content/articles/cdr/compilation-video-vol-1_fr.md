Title: compilation video vol.1
Image: dada_v_001
Price: 10 euros
Tags: mail-order
Date: 2006-01-01 00:00
Keywords: samizdat, compilation, commander, DIY, Creative Commons, libre
Slug: compilation-video-vol-1
Lang: fr
Summary: Après les compilations audio, voici la première compilation vidéo de dadaprod. Vous y retrouverez (au format Dvd) une compilation de reportages diffusés sur le samizdat de dadaprod, qui sont le reflet d'un mouvement social en marche: en lutte contre la marchandisation de nos vies, la folie sécuritaire... En plus de soutenir l'association dadaprod, la diffusion de cette compilation est également un acte de résistance aux médias dominants qui ne semblent s'intéresser à la France d'en bas que pour s'en moquer ou la croquer de propos misérabilistes..
