Title: Dvd / VHS Liability
Image: liability_06_02_04_brazil
Price: 6 euros
Tags: mail-order
Date: 2003-01-01 00:00
Keywords: Brazil, concert, Liability, DVD, montage vidéo, DIY, Creative Commons, libre
Slug: Dvd-VHS-Liability
Lang: fr
Summary: A partir du montage vidéo utilisé pour accompagner le concert de Brazil lors du festival organisé par le webzine Liability. On a ajouté des morceaux live de brazil. Le tout est disponible au format Dvd ou V.H.S
