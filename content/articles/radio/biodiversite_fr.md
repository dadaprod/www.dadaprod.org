Title: biodiversité
Date: 2008-10-25 00:00
Category: radio
Keywords: biodiversité, semences paysanne, environnement, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: biodiversite
Lang: fr
Summary: Rencontre autour du fichage génétique et la privatisation du vivant...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/biodiversite.html){:onClick="ff=window.open('/theme/lecteur/biodiversite.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (13 Mo)](http://www.archive.org/download/PanthereRouge-Biodiversite/dadaprod.radio_biodiversite_CC.by.sa_2008.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

biodiversité
{: .titre .text-center}

Un collectif d'associations nationales a lancé un appel et organisé les
27 et 28 octobre à Paris des rencontres et débats publics sur ce thème.

### SEMONS LA BIODIVERSITE
#### contre le fichage génétique et la privatisation du vivant

Localement pour illustrer symboliquement ce début de campagne, une rencontre
a eu lieu le samedi 25 octobre à 10 h chez Chantal et Jean-Sébastien Gascuel
agriculteurs au Domaine des Raux à Gerzat

C'est là-bas que les micros de la panthère rouge sont allés se poser..

Copyright © 2008 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
