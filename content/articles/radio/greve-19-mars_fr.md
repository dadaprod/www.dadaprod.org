Title: Grève du 19 mars 2009
Date: 2009-03-19 00:00
Category: radio
Keywords: grève, Clermont-Ferrand, reportage, radio, dadaprod, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: greve-19-mars
Lang: fr
Summary: Ballade audio un autre regard sur la crise économique...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/greve-19-mars.html){:onClick="ff=window.open('/theme/lecteur/greve-19-mars.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (7 Mo)](http://www.archive.org/download/GreveDu19Mars2009AClermontFerrand/dadaprod.radio.greve.19.mars_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Grève du 19 mars 2009
{: .titre .text-center}

Une fois de plus dans les cortèges de Clermont Ferrand, dadaprod vous propose
quelques témoignages qui apportent un autre regard sur la crise économique...

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
