Title: RESF parrainage
Date: 2009-01-24 00:00
Category: radio
Keywords: parrainage, RESF, sans papiers, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: RESF-parrainage
Lang: fr
Summary: Captation audio sur des parrainages citoyens organisés par RESF...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/RESF-parrainage.html){:onClick="ff=window.open('/theme/lecteur/RESF-parrainage.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (7 Mo)](http://www.archive.org/download/ResfParrainage/dadaprod.radio_RESF.parrainage_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

RESF parrainage
{: .titre .text-center}

Certains citoyens français ont souhaité marquer leur résistance face à la
politique actuelle de l’immigration si injuste et liberticide en devenant
“parrain” ou“ marraine” d’une personne Sans-Papiers ou demandeur d’asile.

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
