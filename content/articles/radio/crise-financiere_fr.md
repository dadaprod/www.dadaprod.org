Title: La crise financière
Date: 2008-10-10 00:00
Category: radio
Keywords: crise financière, capitalisme, libéralisme, croissance, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: crise-financiere
Lang: fr
Summary: Détournement musical des poncifs échangés sur les media de masse sur la crise financière...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/crise-financiere.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/crise-financiere.html){:onClick="ff=window.open('/theme/lecteur/crise-financiere.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (5 Mo)](http://download.tuxfamily.org/dadaprod/radio/\[dadaprod\].\[radio\].la.crise.financiere.CC.by.nc-sa_2008.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

La crise financière
{: .titre .text-center}

La tentation est assez grande pour un anti-capitaliste de balayer d'un revers
de la main toute réflexion sur la crise actuelle, en se disant qu'après tout,
ils l'ont bien méritée, tant pis pour leur profit, etc.

Plutôt qu'un long discours, voici un détournement musical des poncifs échangés
sur les media de masse sur la crise financière...

Copyright © 2008 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
