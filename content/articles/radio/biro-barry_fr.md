Title: Biro Barry
Date: 2009-10-11 00:00
Category: radio
Keywords: Guinée, Afrique, meurtres, assassinats, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: biro-barry
Lang: fr
Summary: Captation d'une rencontre avec Biro Barry, qui nous parle notamment d'alimentation et de la Guinée...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/biro-barry.html){:onClick="ff=window.open('/theme/lecteur/biro-barry.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (14 Mo)](http://www.archive.org/download/BiroBarry/dadaprod.radio.Biro.Barry_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Biro Barry
{: .titre .text-center}

La panthere NRV a pu rencontré Biro Barry dans le cadre de foire éco-biologique
de Bio 63 à la mairie de Pont-du-Château, le dimanche 11 octobre 2009.

L'engagement de ce représentant de la société civile de Guinée va effet
bien au delà des questions alimentaire. C'est donc l'occasion de faire le
point avec lui sur les évènements récents qui ont agités la Guinée en septembre 2009.

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
