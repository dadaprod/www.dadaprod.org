Title: RESF Maissa
Date: 2008-12-15 00:00
Category: radio
Keywords: RESF, Maissa, expulsion, sans papiers, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: RESF-maissa
Lang: fr
Summary: Captation d'une manifestation de soutien à une étudiante menacée d'expulsion...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/RESF-maissa.html){:onClick="ff=window.open('/theme/lecteur/RESF-maissa.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (4 Mo)](http://www.archive.org/download/RESF-Maissa/dadaprod.radio_RESF.Maissa_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

RESF Maissa
{: .titre .text-center}

Maissa, étudiante en 3e année conditionnelle de licence Mathématiques et
Statistique appliquées à l’économie à l’université Blaise Pascal, risque
plus que jamais d’être expulsée à la suite d’une obligation à quitter le
territoire français. C’est aussi le cas d’autres étudiants.

La loi dite des « libertés et responsabilités des Universités », combattue
par le mouvement des étudiants et des personnels, affirme l’autonomie des
universités françaises. Pourtant, ce n’est pas l’université qui décide de
l’inscription des étudiants étrangers, mais la Préfecture, à laquelle les
lois CESEDA et Hortefeux donnent le pouvoir de juger des parcours universitaires
des étudiants et arrogent des prérogatives universitaires. Le mouvement des
étudiants et des personnels a rejoint le combat de RUSF et RESF en affirmant
dans des motions envoyées au Ministère de l’Enseignement supérieur et de la
Recherche sa volonté de rendre à l’Université ses droits à l’inscription
des étudiants étrangers selon des critères pédagogiques.

sources [RESF 63](http://resf.rusf63.free.fr/){: target="_blank"}

Copyright © 2008 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
