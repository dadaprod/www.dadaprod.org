Title: Rêve général
Date: 2009-01-29 00:00
Category: radio
Keywords: crise, capitalisme, grève, éducation, convention 66, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: reve-generale
Lang: fr
Summary: Crise financière, crise écologique, crise économique, crise du capitalisme...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/reve-generale.html){:onClick="ff=window.open('/theme/lecteur/reve-generale.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (14 Mo)](http://www.archive.org/download/ReveGeneral/dadaprod.radio.reve.general_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Rêve général
{: .titre .text-center}

Crise financière, crise écologique, crise économique, crise du capitalisme...

Nous sommes en 2009 en France, le 29 janvier à Clermont-Ferrand précisément
et il y a eu pas mal de monde dans la rue pour parler de l'éducation, de
l'hôpital en crise, de la convention 66; Mais aussi du président Sarkozy,
des lois libeticides.

Ensemble nous avons échangé dans la joie et la convivialité. Bien loin de la
grogne évoqué par les médias dominants, nous avons envisagé tranquillement
la sortie du capitalisme. 

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
