Title: Hadopi - droits d'auteurs vs Internet
Date: 2009-07-16 00:00
Category: radio
Keywords: Hadopi, droit d'auteur, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: hadopi-droits-auteurs-vs-internet
Lang: fr
Summary: Captation d'une série de tables rondes organisés par le Transfo dans le contexte du projet de loi Hadopi....

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/hadopi.html){:onClick="ff=window.open('/theme/lecteur/hadopi.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=400');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[sources audio](http://www.archive.org/details/Hadopi){: target="_blank"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Hadopi - droits d'auteurs vs Internet
{: .titre .text-center}

Une série de tables rondes organisés par [le Transfo](https://www.letransfo.fr/){: target="_blank"}
dans le contexte du projet de loi Hadopi...

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
