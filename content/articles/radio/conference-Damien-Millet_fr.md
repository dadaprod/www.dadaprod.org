Title: Conférence de Damien Millet
Date: 2012-03-30 00:00
Category: radio
Keywords: CAC, dette, audit citoyen, Creative Commons, libre
Slug: conference-Damien-Millet
Lang: fr
Summary: Voici un montage vidéo (clip) réalisé à partir de différentes sources du Web et bien sûr de la chanson Triple A (AAA)...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/cac.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/cac.html){:onClick="ff=window.open('/theme/lecteur/cac.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.mp3  (86 Mo)](http://archive.org/download/ConferenceDeDamienMilletSurLauditCitoyen/dadaprod_radio_damien-millet_20120330_CC-by-sa_2012.mp3){:download}
[.ogg  (133 Mo)](http://www.archive.org/download/ConferenceDeDamienMilletSurLauditCitoyen/dadaprod_radio_damien-millet_20120330_CC-by-sa_2012.ogg){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Conférence de Damien Millet
{: .titre .text-center}

En guise d'introduction à la conférence de Damien Millet sur l'audit citoyen, donnée à Clermont Ferrand le 30 mars 2012, voici un billet du conférencier :

*Depuis le début des années 1980, les pays du tiers-monde sont confrontés à la crise de la dette et à ses conséquences. Le FMI et leurs créanciers ont imposé des plans d’ajustement structurel qui ont maintenu ces économies dans la soumission, provoqué des ravages sociaux pour les peuples et ouvert la voie aux profits pour les sociétés transnationales qui pouvaient venir sans entraves rafler des parts de marché face aux entreprises locales.*

*Jusqu’au milieu des années 2000, cette logique a perduré. La hausse des cours des matières premières à partir de 2004-2005 a permis aux pays exportateurs d’engranger des réserves de change qu’ils ont souvent utilisées pour se débarrasser de la tutelle encombrante du FMI : Brésil, Argentine, Uruguay, Philippines, Indonésie, tous l’ont remboursé de manière anticipée. Aucun pays ne s’est engagé en profondeur dans la mise en place d’un modèle économique alternatif au capitalisme actuel qui mène l’humanité dans le mur, tant sur le plan social qu’environnemental, même si l’Argentine et l’Equateur ont donné du fil à retordre aux créanciers. De décembre 2001 à mars 2005, l’Argentine a suspendu le remboursement de 90 milliards de dollars et a tenu tête à ses créanciers privés qui ont dû accepter de perdre 65% de la valeur des créances qu’ils détenaient. En 2008, après un audit commandé par le président Rafael Correa, l’Equateur a refusé de rembourser 70% de sa dette privée jugée illégitime, qu’il a finalement rachetée à 35% de sa valeur : le gouvernement a ainsi économisé 7 milliards de dollars qu’il a pu réinvestir dans les dépenses sociales.*

*Depuis 2007-2008, la crise frappe au Nord, et l’Europe est le continent le plus touché. Les peuples européens doivent tirer les enseignements des souffrances endurées par ceux du Sud depuis trois décennies. Au Nord comme au Sud, le discours dominant culpabilise les peuples qui vivraient au-dessus de leurs moyens. Partant de ce constat, l’unique solution proposée est terrible : l’austérité généralisée, des sacrifices innombrables, une sévère détérioration des conditions de vie, dans le seul but de garantir le remboursement de la dette aux créanciers.*

*Pourtant, dans la vie courante, tous nos paiements sont effectués sur présentation d’une facture qui atteste des marchandises achetées ou des services rendus en échange. Dans le cas de la dette publique, où est la facture ? Si dette il y a, elle provient de trois causes : la hausse des taux d’intérêt au début des années 1980, la contre-réforme fiscale qui a permis de réduire de manière importante la fiscalité portant sur les individus les plus riches et les bénéfices des sociétés, et la crise actuelle provoquée par les banques et autres institutions financières privées. Les peuples n’y sont pour rien, ils ne vivent pas au-dessus de leurs moyens puisque les droits humains fondamentaux ne sont souvent pas totalement garantis, et la facture présentée ne correspond pas à des biens ou des services qui leur ont profité. Ce sont les créanciers qui vivent au-dessus de leurs moyens, pas les peuples. Devons-nous rembourser leur dette ?*
