Title: Entretien avec Ignacio Ramonet
Date: 2009-02-19 00:00
Category: radio
Keywords: Le krach parfait, Ignacio Ramonet, crise financière, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: entretien-avec-ignacio-ramonet
Lang: fr
Summary: Analyse de la crise financière et des éventuelles conséquences, sociales et géopolitiques, qui pourraient résulter de ce krach...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/ignacio-ramonet.html){:onClick="ff=window.open('/theme/lecteur/ignacio-ramonet.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (12 Mo)](http://www.archive.org/download/EntretienAvecIgnacioRamonet/dadaprod.radio_Ramonet_CC.by.sa_2009.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Entretien avec Ignacio Ramonet
{: .titre .text-center}

Dans son livre "Le krach parfait", l'ancien directeur du Monde Diplomatique,
Ignacio Ramonet décrit comment se sont mis en place depuis plusieurs décennies,
les éléments qui ont favorisé l'explosion de la crise qui sévit depuis l'automne.
Il analyse les éventuelles conséquences, sociales et géopolitiques, qui
pourraient résulter de ce krach, et propose une liste de mesures pour
refonder l'économie sur des bases plus démocratiques. 

Copyright © 2009 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
