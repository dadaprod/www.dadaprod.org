Title: La crise alimentaire
Date: 2008-10-16 00:00
Category: radio
Keywords: crise alimentaire, émeute de la faim, FAO, dadaprod, radio, gratuit, militant, vorbis, ogg, Creative Commons, libre
Slug: crise-alimentaire
Lang: fr
Summary: Captation d'une conférence sur la crise alimentaire...

<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/radio/nologo.png)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/crise-alimentaire.html){:onClick="ff=window.open('/theme/lecteur/crise-alimentaire.html','dada_a','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=380,height=170');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[.ogg  (52 Mo)](http://www.archive.org/download/LaCriseAlimentaire/dadaprod.radio.crise.alimentaire_CC.by.sa_2008.ogg){:download}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

La crise alimentaire
{: .titre .text-center}

En 2008, la FAO (Organisation des Nations Unies pour l'Alimentation et l'Agriculture)
estime à 925 millions le nombre de personnes souffrant de la faim dans le monde,
soit 75 millions de plus qu'il y a 3 ans. Les Objectifs du Millénaire visant à
réduire de moitié la population souffrant de la faim d'ici à 2015 sont sérieusement remis en question.

En 2008, les émeutes de la faim ont mis en lumière la tragédie qui secoue
particulièrement les populations du Sud: une augmentation de plus de 50%
des prix des denrées alimentaires.

Le 16 octobre 2008, dans le cadre de la Journée Mondiale de l'Alimentation,
Action contre la Faim, A.N.I.S. Etoilé (Agriculture, Nutrition, Interculturel
et Solidarité), le Comité Catholique contre la Faim et pour le Développement
(CCFD) proposent une Conférence Débat sur ce thème.

Cette conférence a été animée par Philippe Boucheix avec la participation de :

1. Pierre VAUQUELIN (CCFD)
2. Catherine Araujo Bonjean, Chargée de Recherche CNRS, CERDI - université d'Auvergne
3. Julien Chalimbaud, responsable de programmes de Sécurité Alimentaire pour Action contre la Faim,
4. Céline Porcheron, Ingénieur en agronomie tropicale membre d'A.N.I.S. Etoilé

Copyright © 2008 dadaprod

**Copyleft** : cette oeuvre est libre, vous pouvez la redistribuer et/ou
la modifier selon les termes de la Licence [Creative Commons by sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/deed.fr){: target="_blank"}
/ [Art Libre](http://artlibre.org){: target="_blank"}.

![cc by sa 3.0](http://www.dadaprod.org/images/logos/cc-by-sa_88x31.png)
{: .text-center}
