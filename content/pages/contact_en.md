Title: Contact
Slug: contact
Lang: en
Summary: Contact page...

Before contacting us, please first read [about our project]({filename}/pages/about_en.md), then listen to our artists...

If you would like to order a disc or send us a demo, first write at :

![E-Mail]({static}/images/mail.jpg)

In any case, feel free to let us know about your point of view or any proposal you would like to suggest...
