Title: Edito - collaboratif label, independant media
Slug: edito
Url: index-en.html
Lang: en
Keywords: label, network, music, Cd-R, mp3, ogg, dada, media, video, michel sardon, brazil, banga, malaussene, anti-capitalism, indy, indie
Summary: Home page...
save_as: index-en.html

Dadaprod still resist for a Internet without GAFAM, publishing free media.
A couple of years ago, we have changed for our hosting platform :
TuxFamily are using more free software. There were hosting our website
without asking money for (we invite you to help them with your gift).
We have now switch to Framagit pages.

#### [Music]({category}album) {:.text-center}
It's been a couple of months since Michel Sardon has not published news
songs for his next album, still under construction, but you can already
listen to it [on line]({filename}/articles/album/les-champs-de-la-resistance_en.md)
and even order it in advance...

#### [Samizdat Tv]({category}samizdat) {:.text-center}
Still more videos covering activism. Last ones are a march for climat
and the Panama Wash action, against tax evasion...
