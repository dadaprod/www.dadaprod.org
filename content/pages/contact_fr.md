Title: Contactez-nous
Slug: contact
Lang: fr
Summary: Page de contact...

Avant de nous contacter, veuillez tout d'abord lire la [présentation de notre projet]({filename}/pages/about_fr.md), puis écoutez [nos artistes]({category}/artiste).

Que vous souhaitiez commander un disque ou envoyer une démo, écrivez-nous
à l'adresse suivante :

![E-Mail]({static}/images/mail.jpg)

N'hésitez pas non plus à nous faire part de vos réactions et autres propositions,
ainsi que de tout autre point de vue pouvant être lié à ce site... 
