Title: Edito - réseaux d'artistes autoproduits, label collaboratif, média indépendant
Slug: edito
Url: index.html
Lang: fr
Keywords: label, réseau, musique, Cd-R, mp3, ogg, dada, media, video, michel sardon, brazil, banga, malaussene, anti-capitalisme, indépendant
Summary: Page d'accueil...
save_as: index.html

Dadaprod continue son œuvre de résistance sur Internet, en vous proposant
encore et toujours des media libres, sans l'aide des GAFAM. Il y a déjà
quelques années, nous avions changé d'hébergeur; Là encore c'était en
rapport avec le monde libre, puisque l'hébergeur était la TuxFamily, qui
avait accepté d'accueillir le site et ce sans contrepartie financière obligatoire.
Nous sommes désormais [hébergés](https://framagit.org/dadaprod/www.dadaprod.org){: target="_blank"}
sur les pages de Framagit...

#### [Musique]({category}album) {:.text-center}
Le dernier album de Michel Sardon est toujours en construction. En plus de pouvoir
l'[écouter en avant première]({filename}/articles/album/les-champs-de-la-resistance_fr.md), vous pouvez également le [pré-commander]({filename}/pages/contact_fr.md)...

#### [Clip Vidéo]({category}video) {:.text-center}
Nous avons également réalisé un [montage vidéo]({filename}/articles/video/le-triple-A_fr.md)
à partir de la deuxième version du morceau Triple A (AAA)  de Michel Sardon.

#### [Samizdat Tv]({category}samizdat) {:.text-center}
Vous retrouverez dans cette section des publications de reportages vidéos
militants, par exemple, celui sur la [marche mondiale pour le climat]({filename}/articles/samizdat/marche-mondiale-pour-le-climat_fr.md),
ou l'opération Panama Wash, contre l'évasion fiscale...
