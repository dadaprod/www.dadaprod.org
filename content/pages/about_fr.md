Title: À propos - réseaux d'artistes autoproduits, label collaboratif, média indépendant
Slug: about
Lang: fr
Keywords: label, réseau, musique, Cd-R, mp3, ogg, dada, media, video, michel sardon, brazil, banga, malaussene, anti-capitalisme, indépendant
Summary: Présentation de Dadaprod...

Dadaprod est un **label collaboratif**. C'est-à-dire un réseau d'artivistes auto-produits (audio/vidéo) réunis autour d'une démarche politique et artistique commune, qui ont décidé de mutualiser leurs moyens afin de contrer le système en place.

Fort de tous ces talents, dadaprod se propose de :

- réaliser les parties sonores et vidéos de vos films (à l’exception des publicités) et spectacles
- réaliser ou co-produire vos films documentaires / fictions

Pour les artistes / Activistes souhaitant rejoindre le réseau. Dadaprod
propose la création d'un **mini-site** (comprenant l'hébergement des médias
autoproduits), d'une adresse mail, d'un espace de communication au travers du site de dadaprod.org.

Dadaprod se présente ainsi comme une **alternative** aux entreprises Web2.0,
qui vous propose des hébergements gratuits en échange du renoncement à vos droits...

Dadaprod propose en plus de prolonger la coopération entre ses différents
membres en éditant des **compilations** (Cd, Dvd) et en favorisant l'organisation
(autogéré) de **plateau** concerts.

Pour les mélomanes et les amateurs de culture libre, nous vous encourageons à
sortir de la passivité que nous impose le système en devenant acteur culturel
et en vous engageant concrètement au côté du collectif d'artistes dans la
distribution et l'organisation de concerts.

Comme tout label, dadaprod possède une direction artistique (dadaprod n'est
pas un site communautaire fourre-tout à la sauce réseau social web 2.0).
Cette direction est assurée par une commission issue de ses membres.

Vous trouverez [ici]({static}/docs/presentation.pdf) une présentation plus
détaillée de la charte et des objectifs de dadaprod et [là]({static}/docs/decroissance.pdf)
une réflexion sur la musique libre et la décroissance

Dadaprod est de plus, une association loi 1901. Vous trouverez ici
les [statuts]({static}/docs/statuts.pdf) de l'association.
