(2004)                      | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
--------------------------- | ------------------------------------ | ------------------------------
Malaussene {:class="titre"} | Dobranoc                             | [![play]({static}/images/boutMP3play.png)](http://danydubois.free.fr/dadaprod/audio/mp3/dobranoc.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

A few years ago, a website dedicated to Dominique A [Comment certains vivent](https://www.commentcertainsvivent.com/){: target="_blank" }
organized a remix contest for the song Dobranoc from Dominique's album
« tout sera comme avant ». The principle was simple
each Internet user could download an mp3 containing a recording of the vocal
track of the song. They were then free to do what they liked with it...
In his own way, Malaussene tried to make Dominique A's distinctive voice
his own and proposed the track to the jury, who ultimately rejected it...
