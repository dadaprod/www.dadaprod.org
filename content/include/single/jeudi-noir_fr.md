(2010)                         | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
------------------------------ | -------------------------------------------- | -------------------------------
Michel Sardon {:class="titre"} | Jeudi noir                                   | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-Jeudi.Noir_Art.Libre_fr_2010.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

A l'occasion de l'expulsion des squatteurs de la place des Vosges à Paris, dont la cour
d'appel venait d'ordonner un jour avant (22 octobre 2010) l'expulsion sans délai,
Michel Sardon a réalisé cette parodie d'un groupe bien connu des années 80.

Cette chanson punk-rock est publiée selon les termes de la [licence](http://www.artlibre.org/){: target="_blank" }
Art Libre. La parodie permet en effet de se réapproprier une œuvre « sacemisé ».

*Ce morceau a été produit avec des logiciels libres (Ardour, Rosegarden, Hydrogen...)
notamment disponibles sur la distribution dédiée [64studio](https://64studio.com/){: target="_blank" }.*
