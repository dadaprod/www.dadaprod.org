(2019)                         | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
------------------------------ | -------------------------------------------- | -------------------------------
Michel Sardon {:class="titre"} | Balance ton point                       | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/balance-ton-point_Michel-Sardon_dadaprod_2019.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/balance-ton-point_Michel-Sardon_dadaprod_2019.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

Si une chanson peut aider à mobiliser alors ce détournement musical n'aura
pas été totalement inutile. Ce morceau provoquera ou non le rire;
En tout cas, c'est sur l'exception de parodie qu'il est construit...
Les paroles sont en licence Creative Commons by sa 3.0 fr / Art Libre. Le morceau est bien sûr versé dans le domaine public.
