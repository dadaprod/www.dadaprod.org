(2006)                         | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
------------------------------ | -------------------------------------------- | -------------------------------
Michel Sardon {:class="titre"} | Stop DRM                                     | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/\[dadaprod\].Banga-stopdrm.cc.by.nc-sa.2.0.fr_2006.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

Après s'être accaparé le marché du disque en se servant notamment de la
consommation de signes, qui se propage par la copie, l'imitation. Le lobby
du disque veut maintenant contrôler les copies privées, quitte à enfreindre
nos libertés individuelles. Quoi de mieux que répondre à cette appropriation
de culture en musique, c'est en tout cas ce que vous propose Michel Sardon
et son Banga looping band avec le single « stop DRM ».
