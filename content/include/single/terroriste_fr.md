(2003)                  | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
----------------------- | -------------------------------------------- | -------------------------------
Brazil {:class="titre"} | terroriste                                   | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Brazil_Terroriste_cc-by-sa_fr_dadaprod_2003.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

Voici deux ans déjà qu’est sorti le Cd-R « la faim du travail ? ».
Longue période durant laquelle Brazil a eu le temps de faire évoluer ses morceaux,
comme ont pu le constater les rares personnes qui ont eu l’occasion d’assister aux performances live de Brazil.
Cette période de répétitions a également été propice à la création de nouveaux morceaux,
en témoigne le nouveau single mp3 terroriste qui nous présente le projet au sommet de son art,
ayant su tirer parti de ces expériences live, le morceau gagne en fluidité,
permettant ainsi au projet de proposer un post-rock plus personnel qui sonnerait
comme une improbable rencontre musicale entre l’univers de Dominique Petitgand
et les dérives soniques des éternels engagés godspeed… 
