(2003)                  | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
----------------------- | ------------------------------------ | ------------------------------
Brazil {:class="titre"} | terroriste                           | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Brazil_Terroriste_cc-by-sa_fr_dadaprod_2003.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

It's already been two years since the release of the Cd-R « La faim du travail ? »
A long time during which Brazil takes time to develop his songs, as the few
who have had the opportunity to attend Brazil's live performances have been able to see for themselves.
This period of rehearsal was also conducive to the creation of new tracks,
as evidenced by the new mp3 single terroriste, which showcases the project at the peak of its powers,
Having learned from these live experiences, the track gains in fluidity,
the project to propose a more personal post-rock sounding like an improbable
improbable musical encounter between the world of Dominique Petitgand
and the sonic excesses of the eternally committed godspeed...
