(2019)                         | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
------------------------------ | ------------------------------------ | ------------------------------
Michel Sardon {:class="titre"} | Balance ton point                    | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/balance-ton-point_Michel-Sardon_dadaprod_2019.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/balance-ton-point_Michel-Sardon_dadaprod_2019.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

A track to motivate the strikes for social rights...
