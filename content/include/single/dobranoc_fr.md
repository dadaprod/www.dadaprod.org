(2004)                      | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
--------------------------- | -------------------------------------------- | -------------------------------
Malaussene {:class="titre"} | Dobranoc                                     | [![play]({static}/images/boutMP3play.png)](http://danydubois.free.fr/dadaprod/audio/mp3/dobranoc.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

Il y a quelques années de cela, un site consacré à Dominique A [Comment certains vivent](https://www.commentcertainsvivent.com/){: target="_blank" }
a organisé un concours de remix sur la chanson Dobranoc figurant sur l'album
de dominique « tout sera comme avant ». Le principe était simple, chaque
internaute pouvait télécharger un mp3 contenant l'enregistrement de la piste
vocale du morceau. Libre à chacun ensuite d'en faire ce qu'il voulait...
Malaussene a, à sa manière, tenté de s'approprier la voix si particulière de
dominique A et a proposé ce morceau au jury, qui ne l'a en dernier lieu pas retenu... 
