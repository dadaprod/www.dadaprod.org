(2010)                         | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
------------------------------ | ------------------------------------ | ------------------------------
Michel Sardon {:class="titre"} | Jeudi noir                           | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-Jeudi.Noir_Art.Libre_fr_2010.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}

On the occasion of the eviction of squatters from the Place des Vosges in Paris,
Michel Sardon created this parody of a well-known 80s band.

This punk-rock song is published under the terms of the Art libre [license](http://www.artlibre.org/){: target="_blank" }.
Parody allows you to reappropriate a "sacemized" work.

*The song was produced using free software (Ardour, Rosegarden, Hydrogen...)
available on the dedicated [64studio](https://64studio.com/){: target="_blank" } distribution.*
