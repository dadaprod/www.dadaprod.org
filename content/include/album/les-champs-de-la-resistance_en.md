<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_006.jpg)
<br>
[zoom]({static}/images/cdr/dada_006.png){:onClick="ff=window.open('/images/cdr/dada_006.png','dada2','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=250,height=250,background-color=red');ff.focus();return(false)"}
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
Les champs de la résistance (liberterre)
{:class="titre"}
</div>
</div>

(2016)                         | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
------------------------------ | ------------------------------------ | ------------------------------
Michel Sardon {:class="titre"} | descendons dans la rue               | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/Michel-Sardon_descendons-dans-la-rue_2_cc-by-sa_fr_2012.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Michel-Sardon_descendons-dans-la-rue_2_cc-by-sa_fr_2012.mp3)
                               | AAA                                  | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/Michel-Sardon_AAA_Art-Libre_fr_2012.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Michel-Sardon_AAA_Art-Libre_fr_2012.mp3)
                               | A bas le nucléaire                   | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/Michel-Sardon_A-bas-le-nucleaire_cc-by-sa_fr_2012.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Michel-Sardon_A-bas-le-nucleaire_cc-by-sa_fr_2012.mp3)
                               | Une Charogne                         | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/dadaprod_Michel-Sardon_Une_Charogne_cc-by-nc-sa-2.0_fr_2010.ogg)
                               | La décroissance                      | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/dadaprod_Michel-Sardon_La_Decroissance_cc-by-nc-sa-2.0_fr_2010.ogg)
                               | Frances Farmer                       | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/dadaprod_Michel-Sardon_Frances_Farmer_cc-by-nc-sa-2.0_fr_2009.ogg)
                               | Le Triple A                          | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/Michel-Sardon_Le-Triple-A_Art-Libre_fr_2012.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Michel-Sardon_Le-Triple-A_Art-Libre_fr_2012.mp3)
                               | OGM Pas                              | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/[dadaprod].Michel.Sardon-OGM.Pas.cc.by.nc-sa.2.0.fr_2008.ogg)
                               | Jeudi Noir                           | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/[dadaprod].Michel.Sardon-Jeudi.Noir_Art.Libre_fr_2010.ogg)
                               | Résistance                           | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/Michel-Sardon_Resistance_cc-by-sa_fr_dadaprod_2012.ogg) [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Michel-Sardon_Resistance_cc-by-sa_fr_dadaprod_2012.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
