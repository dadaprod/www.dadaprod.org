<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_001.jpg)
<br>
[zoom]({static}/images/cdr/dada_001.png){:onClick="ff=window.open('/images/cdr/dada_001.png','dada2','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=250,height=250,background-color=red');ff.focus();return(false)"}
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
Premières nouvelles
{:class="titre"}

écouter {:class="tab-titre"}                  | [haut débit](/theme/lecteur/dada_001.html){:onClick="ff=window.open('/theme/lecteur/dada_001.html','dada_p','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=440');ff.focus();return(false)" title="Ecouter le cd en haut débit"}
--------------------------------------------- | --------------------------------------------
téléchargement Cd entier {:class="tab-titre"} | 
</div>
</div>

(2001)                  | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
----------------------- | -------------------------------------------- | -------------------------------
Brazil {:class="titre"} | l'herbe rouge                                | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.herbe.rouge.cc.by.nc-sa.fr_2002.ogg)
                        | Paul, véro, Sven...                          | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Paul,v%C3%A9ro,Sven.et.les.autres.cc.by.nc-sa.fr_2002.ogg)
                        | un samedi après midi...                      | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Un.samedi.apr%C3%A8s.midi.comme.les.autres.cc.by.nc-sa.fr_2002.ogg)
Banga {:class="titre"}  | la rupture                                   | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Dadaprod_Banga_La-Rupture_cc-by_nc-sa_fr_2001.mp3)
                        | le bateau îvre                               | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Dadaprod_Banga_Le-Bateau-Ivre_cc-by_nc-sa_fr_2001.mp3)
                        | Loanna                                       | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Dadaprod_Banga_Loanna_cc-by_nc-sa_fr_2001.mp3)
                        | summer time                                  | [![play]({static}/images/boutMP3play.png)](http://download.tuxfamily.org/dadaprod/audio/mp3/Dadaprod_Banga_Summer-Time_cc-by_nc-sa_fr_2001.mp3)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
