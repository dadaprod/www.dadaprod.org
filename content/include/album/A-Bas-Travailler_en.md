<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_004.jpg)
<br>
[zoom]({static}/images/cdr/dada_004.png){:onClick="ff=window.open('/images/cdr/dada_004.png','dada2','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=250,height=250,background-color=red');ff.focus();return(false)"}
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
A Bas Travailler (live)
{:class="titre"}

listen {:class="tab-titre"}           | [high rate](/theme/lecteur/dada_004.html){:onClick="ff=window.open('/theme/lecteur/dada_004.html','dada_p','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=440');ff.focus();return(false)" title="Ecouter le cd en haut débit"}
------------------------------------- | --------------------------------------------
download full Cd {:class="tab-titre"} | [zip](https://download.tuxfamily.org/dadaprod/archives/\[dadaprod\].Brazil.-.A.Bas.Travailler.\(dada.004\).cc.by.nc-sa.fr_2005.zip)
</div>
</div>

(2005)                  | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
----------------------- | ------------------------------------ | -------------------------------
Brazil {:class="titre"} | terroriste                           | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-terroriste.cc.by.nc-sa.2.0.fr_2005.ogg)
                        | les marchés (contrôlent nos vies)    | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-les.marches.(controlent.nos.vies).cc.by.nc-sa.2.0.fr_2005.ogg)
                        | travail = aliénation                 | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-travail.=.alienation.cc.by.nc-sa.2.0.fr_2005.ogg)
                        | enfance                              | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-enfance.cc.by.nc-sa.2.0.fr_2005.ogg)
                        | dada m                               | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-dada.m.cc.by.nc-sa.2.0.fr_2005.ogg)
                        | brazil                               | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Brazil-brazil.cc.by.nc-sa.2.0.fr_2005.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
