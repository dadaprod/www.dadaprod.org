<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_002.jpg)
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
la faim du travail
{:class="titre"}

écouter {:class="tab-titre"}                  | [haut débit](/theme/lecteur/dada_002.html){:onClick="ff=window.open('/theme/lecteur/dada_002.html','dada_p','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=440');ff.focus();return(false)" title="Ecouter le cd en haut débit"}
--------------------------------------------- | --------------------------------------------
téléchargement Cd entier {:class="tab-titre"} | 
</div>
</div>

(2002)                  | téléchargement morceaux {:class="tab-titre"} | haut débit {:class="tab-titre"}
----------------------- | -------------------------------------------- | -------------------------------
Brazil {:class="titre"} | le travail I (300 appels par jour)           | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Le.travail.I.(300.appels.par.jour).cc.by.nc-sa.fr_2002.ogg)
                        | Paul, véro, Sven...                          | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Paul,v%C3%A9ro,Sven.et.les.autres.cc.by.nc-sa.fr_2002.ogg)
                        | le marché (contôle nos vies)                 | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.le.march%C3%A9.(cont%C3%B4le.nos.vies).cc.by.nc-sa.fr_2002.ogg)
                        | la pause café                                | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.pause.caf%C3%A9.cc.by.nc-sa.fr_2002.ogg)
                        | le travail II                                | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Le.travail.II.cc.by.nc-sa.fr_2002.ogg)
                        | la fin du travail                            | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.la%20fin.du.travail.cc.by.nc-sa.fr_2002.ogg)
                        | l'herbe rouge                                | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.herbe.rouge.cc.by.nc-sa.fr_2002.ogg)
                        | une vie à deux                               | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Une.vie.%C3%A0.deux.cc.by.nc-sa.fr_2002.ogg)
                        | graffiti                                     | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Graffiti.cc.by.nc-sa.fr_2002.ogg)
                        | l'enfance                                    | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.enfance.cc.by.nc-sa.fr_2002.ogg)
                        | un samedi après midi...                      | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Un.samedi.apr%C3%A8s.midi.comme.les.autres.cc.by.nc-sa.fr_2002.ogg)
                        | les maux des autres                          | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.Les.maux.des.autres.cc.by.nc-sa.fr_2002.ogg)
                        | brazil                                       | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_002/%5Bdadaprod%5D.%5BBrazil%5D.La.faim.du.travail.-.brazil.cc.by.nc-sa.fr_2002.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
