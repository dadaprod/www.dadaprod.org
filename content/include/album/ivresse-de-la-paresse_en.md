<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_003.jpg)
<br>
[zoom]({static}/images/cdr/dada_003.png){:onClick="ff=window.open('/images/cdr/dada_003.png','dada2','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=250,height=250,background-color=red');ff.focus();return(false)"}
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
ivresse de la paresse
{:class="titre"}

listen {:class="tab-titre"}           | [high rate](/theme/lecteur/dada_003.html){:onClick="ff=window.open('/theme/lecteur/dada_003.html','dada_p','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=440');ff.focus();return(false)" title="Ecouter le cd en haut débit"}
------------------------------------- | -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
download full Cd {:class="tab-titre"} | [zip](https://download.tuxfamily.org/dadaprod/archives/\[dadaprod\].Malaussene.vs.Banga.-.ivresse.de.la.paresse.\(dada.003\).cc.by.nc-sa.2.0.fr_2004.zip)
</div>
</div>

(2004)                      | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
--------------------------- | ------------------------------------ | -------------------------------
Malaussene {:class="titre"} | Quittons la France                   | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/Quittons_la_France.ogg)
                            | loin de toi                          | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/loin.ogg)
                            | songe des nuits                      | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/songe.ogg)
                            | bus de nuit                          | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/bus.ogg)
                            | Gaston                               | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/Gaston.ogg)
                            | la France a peur                     | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/la_France_a_peur.ogg)
                            | les sans (papiers)                   | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/papiers.ogg)
                            | une vie                              | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/vie.ogg)
                            | femmes                               | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/femmes.ogg)
                            | les supermarchés                     | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/supermarches.ogg)
Banga {:class="titre"}      | travellers united                    | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/united.ogg)
                            | magic bus                            | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/magic_bus.ogg)
                            | George W bush sucks                  | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/bush.ogg)
                            | I've seen the light                  | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/light.ogg)
                            | love                                 | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/love.ogg)
                            | fireworks                            | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/fireworks.ogg)
                            | girls                                | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/girls.ogg)
                            | tomorrow never ends                  | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/tomorrow.ogg)
                            | nowhere                              | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/nowhere.ogg)
                            | u treat me so bad                    | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/treat_me.ogg)
                            | let's groove                         | [![play]({static}/images/boutOGGplay.png)](http://www.archive.org/download/dada_003/groove.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
