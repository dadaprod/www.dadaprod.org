<div class="row" markdown="1">
<div class="col-xs-3" markdown="1">
![Cd logo]({static}/images/cdr/dada_005.jpg)
<br>
[zoom]({static}/images/cdr/dada_005.png){:onClick="ff=window.open('/images/cdr/dada_005.png','dada2','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=250,height=250,background-color=red');ff.focus();return(false)"}
</div>
<div class="col-xs-8 col-xs-offset-1" markdown="1">
descendons dans la rue
{:class="titre"}

listen {:class="tab-titre"}           | [high rate](/theme/lecteur/dada_005.html){:onClick="ff=window.open('/theme/lecteur/dada_005.html','dada_p','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=440');ff.focus();return(false)" title="Ecouter le cd en haut débit"}
------------------------------------- | --------------------------------------------
download full Cd {:class="tab-titre"} | [zip](https://download.tuxfamily.org/dadaprod/archives/\[dadaprod\].Banga.-.descendons.dans.la.rue.\(dada.005\).cc.by.nc-sa.fr_2007.zip)
</div>
</div>

(2007)                         | download tracks {:class="tab-titre"} | high rate {:class="tab-titre"}
------------------------------ | ------------------------------------ | -------------------------------
Michel Sardon {:class="titre"} | descendons dans la rue               | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-descendons.dans.la.rue.cc.by.nc-sa.2.0.fr_2007.ogg)
                               | telechargez moi                      | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-telechargez.moi.cc.by.nc-sa.2.0.fr_2007.ogg)
                               | fuck the 4x4                         | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-fuck.the.4x4.cc.by.nc-sa.2.0.fr_2007.ogg)
                               | sensation                            | [![play]({static}/images/boutOGGplay.png)](http://download.tuxfamily.org/dadaprod/audio/ogg/\[dadaprod\].Michel.Sardon-sensation.cc.by.nc-sa.2.0.fr_2007.ogg)

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
