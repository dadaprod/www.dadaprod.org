<div class="row" markdown="1">
<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/video/aaa.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/aaa.html){:onClick="ff=window.open('/theme/lecteur/aaa.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[. ogv  (35 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod_Michel-Sardon_AAA_2012.ogv){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Voici un montage vidéo (clip) réalisé par Michel Sardon à partir de différentes sources du Web et bien sûr de la chanson **Triple A (AAA)**.

Ce morceau "Triple A (AAA)" est en fait un deuxième arrangement du morceau.
une première version se trouve également sur l'album [les champs de la résistance (liberterre)]({filename}/articles/album/les-champs-de-la-resistance_fr.md)

La musique de Michel Sardon est diffusée en [licence Art Libre](http://artlibre.org/){: target="_blank" }.
Par contre cette vidéo n'a pas de droit bien identifié.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank" }
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

</div>
</div>
