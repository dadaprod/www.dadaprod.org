<div class="row" markdown="1">
<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/cdr/liability_06_02_04_brazil.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/liability.html){:onClick="ff=window.open('/theme/lecteur/liability.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[. ogv  (679 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.video.brazil_2005_512kb.ogv){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

A bas travailler !
{: .titre .text-center}

In 2004, sardon first decided to work on a video support during rehearsals
of Brazil for a gig that happened the same year. The succession of images
shot during sardon's travel and demonstration during the French European
social forum perfectly match to the music of Brazil, which constantly
oscillate between the poetic and politic field. Recently,when dadaprod was
working on the release of the [live Cd]({filename}/articles/album/A-Bas-Travailler_en.md) of Brazil “A bas travailler!”,
we decided to work again on this video, that you can now download...

*If you have difficulty playing the video, we advise you to [install](https://www.videolan.org/vlc/){: target="_blank" }
the VLC player (free software) on your computer.*

</div>
</div>
