<div class="row" markdown="1">
<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/video/4x4.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/4x4.html){:onClick="ff=window.open('/theme/lecteur/4x4.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[. ogv  (36 Mo)](http://www.archive.org/download/FuckThe4x4/dadaprod.video.fuck.the.4x4_2007_512kb.ogv){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Fuck the 4x4
{: .titre .text-center}

Le 13 janvier 2006, le **Paris-Dakar** tuait son 9ème spectateur sur son parcours.
Le Paris-Dakar, fête du 4x4 est une catastrophe humaine et écologique.
Ce rallye symbolise l'attitude irresponsable de quelques gosses de riches qui
dépensent des sommes colossales pour aller les gaspiller sur la terre de ceux
qui n'ont rien. D'un point de vue écologique, c'est une aberration de plus étant
donné l'énergie gaspillée dans autant de futilités.
Les médias dominants, qui d'habitude aiment à souligner les bons gestes écologiques,
abandonnent leur morale au vestiaire afin de mieux célébrer cette course meurtrière
et publicitaire! Le Paris-Dakar est en effet le fer de lance de l'opération de
communication des constructeurs automobiles dont les 4x4 inondent de plus en plus nos centres villes.

A l'aide d'images autoproduites ou tirés d'un reportage vélorutionnaire et de
quelques images du Paris-Dakar, dadaprod a monté un clip vidéo pour illustrer
le dernier titre de **Michel Sardon & le Banga looping band**, sobrement intitulé : « fuck the 4x4 ». 

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank" }
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

</div>
</div>
