<div class="row" markdown="1">
<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/video/aaa.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/aaa.html){:onClick="ff=window.open('/theme/lecteur/aaa.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[. ogv  (35 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod_Michel-Sardon_AAA_2012.ogv){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

Here's a video montage based on various Web sources and, of course, the song **Triple A (AAA)**.

This track "Triple A (AAA)" is in fact a second arrangement of the song.
A first version can also be found on the album
[les champs de la résistance (liberterre)]({filename}/articles/album/les-champs-de-la-resistance_en.md)

Michel Sardon's music is released under an [licence Art Libre](http://artlibre.org/){: target="_blank" }.
However, this video has no clearly identified rights.

*If you have difficulty playing the video, we advise you to [install](https://www.videolan.org/vlc/){: target="_blank" }
the VLC player (free software) on your computer.*

</div>
</div>
