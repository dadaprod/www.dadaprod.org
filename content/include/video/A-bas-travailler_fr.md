<div class="row" markdown="1">
<div class="col-sm-3" markdown="1">
<div class="row" markdown="1">
<div class="col-sm-12 col-xs-6" markdown="1">
![logo]({static}/images/cdr/liability_06_02_04_brazil.jpg)
</div>
<div class="col-sm-12 col-xs-5 col-xs-offset-1 col-sm-offset-0" markdown="1">
<span class="tab-titre">streaming</span><br/>
[lecteur embarqué](/theme/lecteur/liability.html){:onClick="ff=window.open('/theme/lecteur/liability.html','dada_v','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=600,height=450');ff.focus();return(false)"}

<span class="tab-titre">download</span><br/>
[. ogv  (679 Mo)](http://www.archive.org/download/SamizdatTv/dadaprod.video.brazil_2005_512kb.ogv){:download}

![Creative Commons]({static}/images/cc.gif)
{: class="text-center"}
</div>
</div>
</div>
<div class="col-sm-9" markdown="1">

A bas travailler !
{: .titre .text-center}

En perspective d'un concert de **Brazil** (post-rock), Michel Sardon a
commencé à travailler sur un support vidéo afin d'accompagner la musique
instrumentale de son projet solo.
Les différentes séquences d'images tournées à l'occasion de ses pérégrinations
au travers du monde ou lors de manifestations parisiennes se révèlent alors
à l'univers décalé de Brazil, oscillant en permanence entre le champ poétique et politique.
À l'occasion de la sortie du [Cd live]({filename}/articles/album/A-Bas-Travailler_fr.md) de Brazil « A bas travailler! »,
dadaprod ressort ce support vidéo et commence à retravailler sur un nouveau montage vidéo,
que nous vous proposons désormais en téléchargement.

*En cas de difficulté pour lire la vidéo, nous vous conseillons d'[installer](https://www.videolan.org/vlc/){: target="_blank" }
le lecteur VLC (logiciel libre et gratuit) sur votre ordinateur.*

</div>
</div>
