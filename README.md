# Site www.dadaprod.org

Le site est généré via le moteur d'intégration continue de Gitlab-CE.
Il est hébergé sur les pages de framagit.

Le thème est un thème original, développé par Dadaprod.

## Installation

Après avoir téléchargé ou cloné le projet, vous pouvez installer le site statique de plusieurs façons.

### Dans un environnement virtuel Python
Lancez d'abord la commande :
```
pip install -r requirements.txt
```
Exécutez ensuite le serveur de développement :
```
pelican -lr
```
Le site est alors disponible à l'adresse `http://localhost:8000/`.

### Sur une distribution GNU/Linux
Il est nécessaire d'installer le paquet **Pelican**, par exemple sur une distribution Debian, en exécutant la commande :
```
apt install pelican
```
Il sera ensuite nécessaire d'installer l'extension `pelican-markdown-include`,
qui n'est pas présente dans les dépôts (un environnement virtuel Python
paraît opportun).

Une fois cela fait, exécutez ensuite le serveur de développement :
```
pelican -lr
```
Le site est alors disponible à l'adresse `http://localhost:8000/`.

### Dans un environnement Docker
Vous pouvez construire l'image à l'aide du fichier `Dockerfile` :
```
docker build -t debian-pelican .
```
Puis, vous exécutez cette image dans un conteneur :
```
docker run -d -p 80:80 debian-pelican
```
Le site est alors disponible à l'adresse `http://localhost`.

## License
* Le contenu du site est soumis à différentes licences ouvertes (les plus
anciennes contributions ayant souvent une restriction commerciale).
* Les fichiers du thème sous la [licence][ccbysa] Creative commons by sa 3.0.

[ccbysa]: https://creativecommons.org/licenses/by-sa/3.0/fr/
